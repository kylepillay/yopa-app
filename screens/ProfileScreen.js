import React, { useCallback, useEffect, useState } from 'react';
import {
  Alert,
  ScrollView,
  StyleSheet,
  Switch,
  View
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Button, Icon, Input, Text } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";
import authService from '../services/authService';

import { setUserData, logoutUser } from "../redux/auth/actions";
import * as MediaLibrary from 'expo-media-library';
import theme from '../configs/theme';

const ProfileScreen = ({ navigation }) => {
  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.user.data)
  const [status, requestPermission] = MediaLibrary.usePermissions();

  useEffect(() => {
    if (status && status.status !== 'granted' && status.canAskAgain === true) {
      requestPermission()
    }
  }, [status])

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [city, setCity] = useState('')
  const [password, setPassword] = useState('')
  const [password_confirmation, setPassword_confirmation] = useState('')
  const [allow_push_notifications, setAllow_push_notifications] = useState(false)
  const [allow_email_notifications, setAllow_email_notifications] = useState(false)
  const [base64, setBase64] = useState(null)
  const [loading, setLoading] = useState(false);


  useEffect(() => {
    if (user) {
      const { name, email, phone, city, allow_push_notifications, allow_email_notifications, media } = user;
      setImageUri(media?.original_url)
      setBase64(null)
      setName(name)
      setEmail(email)
      setPhone(phone)
      setCity(city)
      setAllow_push_notifications(allow_push_notifications)
      setAllow_email_notifications(allow_email_notifications)
    }
  }, [user])

  const [imageUri, setImageUri] = useState(null)

  const _update = useCallback(() => {
    setLoading(true)

    const dataToSend = {
      ...(base64 && { image: base64 }),
      name: name ? name.toString() : '',
      email: email ? email.toString() : '',
      phone: phone ? phone.toString() : '',
      city: city ? city.toString() : '',
      allow_push_notifications: !!allow_push_notifications,
      allow_email_notifications: !!allow_email_notifications
    }

    authService.updateUserData(dataToSend).then((userData) => {
      dispatch(setUserData(userData))
      setLoading(false)
    }).catch((error) => {
      console.log('error')
      console.log(error)
      setLoading(false)
    })
  }, [name, email, phone, city, allow_email_notifications, allow_push_notifications, base64]);

  const _pickImage = async () => {

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });

    if (!result.cancelled) {
      setImageUri(result.uri);
      setBase64(result.base64)
    }
  };

  const _logOut = () => {
    Alert.alert('Are you sure?', 'You will be logged out of the app.', [
      {
        text: 'Log Out', onPress: () => {
          dispatch(logoutUser());
        }
      },
      {
        text: 'Cancel', onPress: () => {
        }
      }
    ])

  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.header}></View>

        <View style={{ ...styles.listHeaderTextContainer, marginTop: 1 }}>
          <Text style={styles.listHeaderText}>INFORMATION</Text>
        </View>
        <Input
          label='Email Address'
          placeholder='Email Address'
          autoCorrect={false}
          autoCapitalize='none'
          value={email}
          name='email'
          textContentType='emailAddress'
          onChangeText={text => setEmail(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='ios-mail' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <Input
          label='Name'
          placeholder='Name'
          autoCorrect={false}
          autoCapitalize='none'
          value={name}
          name='name'
          textContentType='givenName'
          onChangeText={text => setName(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='ios-person' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <Input
          label='Phone Number'
          placeholder='Phone Number'
          autoCorrect={false}
          autoCapitalize='none'
          value={phone}
          name='phone'
          textContentType='telephoneNumber'
          onChangeText={text => setPhone(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='ios-phone-portrait' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <View style={styles.listHeaderTextContainer}>
          <Text style={styles.listHeaderText}>LOCATION</Text>
        </View>
        <Input
          label='City'
          placeholder='City'
          autoCorrect={false}
          value={city}
          autoCapitalize='none'
          textContentType='addressCity'
          name='city'
          onChangeText={text => setCity(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='ios-compass' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <View style={styles.listHeaderTextContainer}>
          <Text style={styles.listHeaderText}>Password</Text>
        </View>
        <Input
          label="Password"
          placeholder='Password'
          autoCorrect={false}
          value={password}
          autoCapitalize='none'
          textContentType='password'
          name='password'
          onChangeText={text => setPassword(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='lock-closed' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <Input
          label="Re-type Password"
          placeholder='Re-type Password'
          autoCorrect={false}
          value={password_confirmation}
          autoCapitalize='none'
          textContentType='password'
          name='password_confirmation'
          onChangeText={text => setPassword_confirmation(text)}
          inputContainerStyle={styles.inputContainer}
          leftIcon={<Icon name='lock-closed' containerStyle={{ marginRight: 15, }} type='ionicon' size={24} color={theme.colors.primary} />}
        />
        <View style={styles.listHeaderTextContainer}>
          <Text style={styles.listHeaderText}>SETTINGS</Text>
        </View>

        <View style={{ ...styles.switchContainer, marginTop: 20 }}>
          <Text style={styles.switchLabel}>Push Notifications</Text>
          <Switch
            name='allow_push_notifications'
            value={!!allow_push_notifications}
            onValueChange={value => setAllow_push_notifications(value)}
          />
        </View>

        <View style={styles.switchContainer}>
          <Text style={styles.switchLabel}>Email Notifications</Text>
          <Switch
            name='allow_email_notifications'
            value={!!allow_email_notifications}
            onValueChange={value => setAllow_email_notifications(value)}
          />
        </View>

        <Button
          loading={loading}
          onPress={_update}
          loadingProps={{ color: theme.colors.primary }}
          type="outline"
          titleStyle={{ color: theme.colors.primary, marginBottom: 2 }}
          containerStyle={{ marginVertical: 20 }}
          raised
          title='SAVE'
          icon={<Icon name="ios-save" color={theme.colors.primary} type='ionicon' size={26} containerStyle={{ marginRight: 15 }} />}
        />

        <Button
          onPress={_logOut}
          loadingProps={{ color: theme.colors.primary }}
          type="outline"
          titleStyle={{ color: theme.colors.primary, marginBottom: 2 }}
          containerStyle={{ marginVertical: 20 }}
          raised
          title='LOG OUT'
          icon={<Icon name="ios-exit" color={theme.colors.primary} type='ionicon' size={26} containerStyle={{ marginRight: 15 }} />}
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
  },
  header: {
    height: 60,
    backgroundColor: theme.colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listHeaderTextContainer: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: theme.colors.primary,
    marginBottom: 10
  },
  listHeaderText: {
    color: '#969696',
    fontSize: 18,
    fontFamily: 'Rubik-Medium',
    paddingVertical: 5
  },
  switchContainer: {
    padding: 10
  },
  switchLabel: {
    color: theme.colors.primary,
    fontSize: 18,
    marginBottom: 5,
    fontFamily: 'Rubik-Regular'
  },
  inputContainer: {
    borderRadius: 0,
    borderBottomColor: theme.colors.textMedium,
    borderBottomWidth: 1,
    margin: 0
  }
});

export default ProfileScreen;
