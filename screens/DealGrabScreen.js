import * as WebBrowser from 'expo-web-browser';
import MapView from 'react-native-maps';
import React, { useCallback, useState, useEffect } from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Share
} from 'react-native';
import { Badge, Button, Icon, Image, Text } from "react-native-elements";
import { useSelector, useDispatch } from "react-redux";

import { favouriteDeal, favouriteDealReset } from "../redux/deals/actions";
import locationService from '../services/locationService';
import theme from '../configs/theme';

const DealGrabScreen = ({ route }) => {
  const dispatch = useDispatch()

  const favouriteDealState = useSelector(state => state.deals.favourite_deal)
  const location = useSelector(state => state.location.location).coords

  const [map, setMap] = useState(false)
  const [deal, setDeal] = useState({})

  useEffect(() => {
    if (route?.params?.deal) {
      setDeal(route.params.deal)
    }
  }, [route])

  useEffect(() => {
    if (favouriteDealState?.data) {
      dispatch(favouriteDealReset())
      setDeal({ ...deal, favourited: !deal.favourited })
    }
  }, [favouriteDealState, deal])

  const _onShare = async () => {
    try {
      const result = await Share.share({
        message: deal.link,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log(result.activityType)
        } else {
          console.log('Shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('Dismissed')
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const _handlePressButtonAsync = async () => {
    await WebBrowser.openBrowserAsync(deal.link);
  };

  const _favouriteDeal = useCallback(() => {
    dispatch(favouriteDeal(deal))
  }, [deal]);

  const _openMapView = useCallback(() => {
    setMap(true)
  }, [])

  return (
    location ?
      <View style={styles.container}>
        {!map ? <View>
          <View style={styles.header}>
            <Button
              onPress={_openMapView}
              type="outline"
              loadingProps={styles.buttonLoadingProps}
              titleStyle={styles.buttonTitle}
              containerStyle={{ ...styles.buttonContainer, marginTop: 10 }}
              buttonStyle={styles.button}
              raised
              title='TAKE ME THERE'
              icon={<Icon name="ios-navigate" color={theme.colors.primary} type='ionicon' size={26} containerStyle={styles.buttonIcon} />}
            />
            <Button
              onPress={_handlePressButtonAsync}
              type="outline"
              loadingProps={styles.buttonLoadingProps}
              titleStyle={styles.buttonTitle}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              raised
              title='VIEW DEAL ONLINE'
              icon={<Icon name="ios-globe" color={theme.colors.primary} type='ionicon' size={26} containerStyle={styles.buttonIcon} />}
            />
            <Button
              type="outline"
              onPress={_onShare}
              loadingProps={styles.buttonLoadingProps}
              titleStyle={styles.buttonTitle}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              raised
              title='SHARE THIS DEAL'
              icon={<Icon name="ios-share" color={theme.colors.primary} type='ionicon' size={26} containerStyle={styles.buttonIcon} />}
            />
            <Button
              onPress={_favouriteDeal}
              loading={favouriteDealState.isLoading}
              type="outline"
              loadingProps={styles.buttonLoadingProps}
              titleStyle={styles.buttonTitle}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.button}
              raised
              title={deal.favourited ? 'REMOVE FROM FAVOURITES' : 'ADD TO FAVOURITES'}
              icon={<Icon name="ios-heart" color={theme.colors.primary} type='ionicon' size={26} containerStyle={styles.buttonIcon} />}
            />
          </View>
          <ScrollView>
            <View style={styles.dealContainer}>
              <View style={styles.dealHeaderContainer}>
                <Text style={styles.dealHeaderText}>{deal.name}</Text>
              </View>
              <View style={styles.dealSubheaderTextContainer}>
                <Text style={styles.dealSubheaderText}>Price: R {deal.price} / Sale Price: R {deal.salePrice}</Text>
              </View>
              <Image
                source={{
                  uri: `${deal.imageurl}`
                }}
                style={styles.image}
              />
              <Badge
                status="primary"
                textStyle={styles.imageBadgeText}
                value={'You Save: R ' + (deal.price - deal.salePrice).toFixed(2)}
                containerStyle={styles.imageBadgeContainer}
                badgeStyle={styles.imageBadge}
              />
              <View style={styles.bottomContainer}>
                <View style={styles.availableAtContainer}>
                  <Text style={styles.availableAtText}>Available at </Text>
                </View>
                <View style={styles.bottomImageContainer}>
                  <Image
                    source={{
                      uri: `${deal?.store?.imageurl}`
                    }}
                    resizeMode={'contain'}
                    style={styles.bottomImage}
                  />
                </View>
                <View style={styles.distanceTextContainer}>
                  <View style={styles.distanceTextSubContainer}>
                    <Text style={styles.distanceText}>{locationService.distanceBetweenLocations(location.latitude, location.longitude, parseFloat(deal.latitude), parseFloat(deal.longitude))}</Text>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View> : <View style={{ flex: 1 }}>
          {deal?.store?.latitude && map && <MapView
            style={{ flex: 1 }}
            initialRegion={{
              latitude: deal?.store?.latitude ? parseFloat(deal?.store?.latitude) : 0,
              longitude: deal?.store?.longitude ? parseFloat(deal?.store?.longitude) : 0,
              latitudeDelta: 0.0092,
              longitudeDelta: 0.0042,
            }}
          />}

          <TouchableOpacity style={styles.tabBarInfoContainer} onPress={() => { setMap(false) }}>
            <Text style={styles.tabBarInfoText}>Close Map</Text>
          </TouchableOpacity>
        </View>
        }
      </View> :
      <View style={{ ...styles.container, backgroundColor: theme.colors.primary, alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', height: 60, paddingHorizontal: 20 }}>
          <Text style={styles.modalHeader}>Location Services Disabled</Text>
          <Text style={styles.modalText}>Please enable location services to use this feature. If this message still appears try completely closing and reopening this app.</Text>
        </View>
      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.textLight,
  },
  topBarLeftIcon: {
    flex: 1,
    alignSelf: 'flex-start',
    marginLeft: 5
  },
  topBarLeftIconContainer: {
    width: 20,
    backgroundColor: '#439baa'
  },
  topBarCenterText: {
    color: theme.colors.textLight
  },
  modalHeader: {
    color: theme.colors.textLight,
    fontSize: 22,
    marginBottom: 40,
    fontFamily: 'Rubik-Black'
  },
  modalText: {
    color: theme.colors.textLight,
    fontSize: 18,
    fontFamily: 'Rubik-Regular'
  },
  header: {
    height: 265,
    paddingHorizontal: 10,
    backgroundColor: theme.colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  dealContainer: {
    marginVertical: 10,
    flex: 1,
    paddingHorizontal: 10
  },
  dealHeaderContainer: {
    height: 40,
    justifyContent: 'center'
  },
  dealHeaderText: {
    color: theme.colors.primary,
    fontSize: 26,
    marginLeft: 10,
    fontFamily: 'Rubik-Medium'
  },
  dealSubheaderTextContainer: {
    height: 30
  },
  dealSubheaderText: {
    fontFamily: 'Baloo',
    color: theme.colors.textMedium,
    justifyContent: 'center',
    fontSize: 18,
    marginLeft: 10
  },
  image: {
    height: 200,
    marginTop: 10,
    marginHorizontal: 10
  },
  imageBadgeContainer: {
    position: 'absolute',
    top: 70,
    right: 20
  },
  imageBadge: {
    backgroundColor: theme.colors.primary,
    height: 40,
    paddingHorizontal: 10
  },
  imageBadgeText: {
    fontFamily: 'Baloo',
    color: theme.colors.textLight,
    fontSize: 20
  },
  bottomContainer: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    marginTop: 5
  },
  availableAtContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  availableAtText: {
    fontFamily: 'Baloo',
    color: theme.colors.primary,
    fontSize: 20,
    marginLeft: 10
  },
  bottomImageContainer: {
    flex: 1
  },
  bottomImage: {
    height: 50,
    width: 100,
    borderRadius: 5
  },
  distanceTextContainer: {
    flex: 1
  },
  distanceTextSubContainer: {
    marginHorizontal: 10,
    backgroundColor: theme.colors.primary,
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  distanceText: {
    fontFamily: 'Baloo',
    color: theme.colors.textLight,
    fontSize: 18,
    marginLeft: 10
  },
  button: {
    borderRadius: 10,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0
  },
  buttonIcon: {
    marginRight: 20
  },
  buttonLoadingProps: {
    color: theme.colors.primary
  },
  buttonTitle: {
    color: theme.colors.primary,
    marginBottom: 2
  },
  buttonContainer: {
    width: '100%',
    marginHorizontal: 10,
    marginBottom: 15,
    borderRadius: 10
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
    borderRadius: 4,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: theme.colors.primary,
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    fontFamily: 'Rubik-Regular',
    color: theme.colors.textLight,
    textAlign: 'center',
  },
});

export default DealGrabScreen;
