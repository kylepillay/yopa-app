import * as _ from 'lodash';
import React, { useEffect, useState } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    TouchableOpacity,
    Animated
} from 'react-native';
import { Header, Icon, Text } from "react-native-elements";
import { useSelector } from "react-redux";
import Constants from 'expo-constants';

import { useWindowDimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import TabContainer from './components/TabContainer';
import SearchByLocation from './components/SearchByLocation';
import SearchByCategories from './components/SearchByCategories';




const SearchScreen = ({ navigation }) => {

    const [deals, setDeals] = useState([]);
    const [error, setError] = useState(null);

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Price & Location' },
        { key: 'second', title: 'Categories' }
    ]);

    const globalDeals = useSelector(s => s.deals.search_deals.data)

    const FirstRoute = () => {
        return <TabContainer>
            <SearchByLocation navigation={navigation} />
        </TabContainer>
    };


    const SecondRoute = () => {
        return <TabContainer>
            <SearchByCategories navigation={navigation} />
        </TabContainer>
    };

    const renderScene = SceneMap({
        first: FirstRoute,
        second: SecondRoute
    });

    useEffect(() => {
        if (globalDeals && globalDeals.length) {
            // Filter already smashed deals from the search results
            let localDeals = _.filter(globalDeals, { 'smashed': false });
            if (localDeals) {
                setDeals(localDeals)
            }
        }
    }, [globalDeals])

    const layout = useWindowDimensions();

    const _renderTabBar = (props) => {
        const inputRange = props.navigationState.routes.map((x, i) => i);

        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const opacity = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map((inputIndex) =>
                            inputIndex === i ? 1 : 0.5
                        ),
                    });

                    const selected = index === i;

                    return (
                        <TouchableOpacity
                            key={i}
                            style={{
                                flex: 1,
                                alignItems: 'center',
                                padding: 16,
                                backgroundColor: !selected ? '#d7d7d7' : '#ffffff'
                            }}
                            onPress={() => setIndex(i)}>
                            <Animated.Text style={{
                                fontFamily: 'Baloo',
                                fontSize: 18,
                                color: !selected ? '#ffffff' : '#439baa'
                            }}>{route.title}</Animated.Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Search Deals</Text>
                <Text style={styles.subHeaderText}>{deals.length + ' Products'}</Text>
            </View>
            <TabView
                style={{ fontFamily: "Baloo" }}
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                renderTabBar={_renderTabBar}
                initialLayout={{ width: layout.width }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tabBar: {
        flexDirection: 'row'
    },
    pageHeadings: {
        fontSize: 20,
        fontFamily: 'Baloo',
        color: '#5c5c62'
    },
    header: {
        height: 80,
        backgroundColor: '#439baa',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontSize: 24,
        fontFamily: 'Rubik-Black'
    },
    subHeaderText: {
        color: 'white',
        fontSize: 14,
        fontFamily: 'Rubik-Regular'
    },
    cardHeader: {
        flex: 1,
        color: '#439baa',
        fontSize: 22,
        marginLeft: 5,
        alignSelf: 'flex-start',
        marginBottom: 10,
        fontFamily: 'Rubik-Black'
    },
    modalHeader: {
        color: '#fff',
        fontSize: 22,
        marginBottom: 20,
        fontFamily: 'Rubik-Black'
    },
    modalText: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { height: -3 },
                shadowOpacity: 0.1,
                shadowRadius: 3,
            },
            android: {
                elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        fontFamily: 'Rubik-Regular',
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});

SearchScreen['navigationOptions'] = ({ navigation }) => {
    return {
        header: <Header
            backgroundColor='#439baa'
            centerComponent={{ text: 'Find Deals', style: { color: '#fff' } }}
            rightComponent={<Icon
                type='ionicon'
                name='ios-heart-empty'
                onPress={() => {
                    navigation.navigate('Favourites')
                }}
                style={{ flex: 1, alignSelf: 'flex-end' }}
                color='#ffffff'
            />}
        />,
    }
};

export default SearchScreen;
