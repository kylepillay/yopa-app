import styled from '@emotion/native'


const TabContainer = styled.View`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
`

export default TabContainer;
