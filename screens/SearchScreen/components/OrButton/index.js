import styled from '@emotion/native'

const OrButton = styled.View`
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #00a1b2;
    border-radius: 5px;
`

const OrButtonText = styled.Text`
    font-size: 24px;
    font-family: 'Baloo';
    color: #FFFFFF;
`

export { OrButtonText }

export default OrButton;