import styled from '@emotion/native'


const ContentContainer = styled.View`
    margin-top: 20px;
    padding: ${props => props.p ? props.p : 0}px;
`

export default ContentContainer;
