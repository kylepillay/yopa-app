import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, Dimensions, FlatList, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'react-native-elements'
import { Picker } from '@react-native-picker/picker';
import Slider from '@react-native-community/slider';
import Heading from '../Heading';
import ContentContainer from '../ContentContainer';
import OrButton, { OrButtonText } from '../OrButton';
import { getAllProvinces, searchLocationDeals } from '../../../../redux/deals/actions';
import DealListItem from '../../../../components/DealListItem';

const SearchByLocation = ({ navigation }) => {

    const windowWidth = Dimensions.get('window').width;

    const dispatch = useDispatch()
    const [deals, setDeals] = useState([]);
    const [distance, setDistance] = useState(1);
    const [price, setPrice] = useState(10);
    const [province, setProvince] = useState(0);
    const [error, setError] = useState(null);

    const provinces = useSelector(s => s.deals.provinces.data)
    const globalDeals = useSelector(s => s.deals.search_deals.data)
    const isLoadingAllDeals = useSelector(s => s.deals.search_deals.isLoading)
    const isRefreshingAllDeals = useSelector(s => s.deals.search_deals.isRefreshing)

    const location = useSelector(s => s.location.location.coords)

    const _searchPriceLocationDeals = () => {
        dispatch(searchLocationDeals(
            location.latitude,
            location.longitude,
            distance,
            price,
            province
        ));
    }

    useEffect(() => {
        if (globalDeals && globalDeals.length) {
            setDeals(globalDeals)
        }
    }, [globalDeals])

    const _renderFooter = () => {
        if (isLoadingAllDeals && !isRefreshingAllDeals) {
            return (
                <View style={styles.tabBarInfoContainer}>
                    <ActivityIndicator animating size="large" />
                </View>
            );
        }
        return null;
    };

    const _onProvinceChanged = (itemValue, itemIndex) => {
        setProvince(itemIndex + 1);
    }

    useEffect(() => {
        dispatch(getAllProvinces())
    }, [])

    return (
        deals && deals.length && deals.length > 0 ? <View style={{ width: '100%' }}><FlatList
            data={deals}
            refreshing={isRefreshingAllDeals}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (<View style={{ marginBottom: 10 }}><DealListItem navigation={navigation} deal={item} /></View>)}
            ListFooterComponent={_renderFooter}
        />
            <View style={{ position: 'absolute', bottom: 10, right: 10 }}><Button onPress={() => setDeals([])} buttonStyle={{ width: 120, height: 45, backgroundColor: '#39464f', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }} title={'Back'} titleStyle={{ color: '#FFFFFF', fontFamily: 'Baloo', fontSize: 18 }} /></View>
        </View>
            : <>
                <ContentContainer>
                    <Heading>Choose Location</Heading>
                </ContentContainer>
                <ContentContainer>
                    <View style={{
                        paddingHorizontal: 50,
                        backgroundColor: '#006f7a',
                        width: windowWidth - 40,
                        height: 60,
                        borderRadius: 40
                    }}>
                        <Picker
                            selectedValue={province} x
                            style={{ height: 50, backgroundColor: '#006f7a', color: '#ffffff', textAlign: 'center' }}
                            dropdownIconColor={'#ffffff'}
                            dropdownIconRippleColor={'#ffffff'}
                            onValueChange={_onProvinceChanged}>
                            {
                                provinces.map((item) => {
                                    return (<Picker.Item label={item.name} value={item.id} key={item.id} />)
                                })
                            }
                        </Picker>
                    </View>
                </ContentContainer>
                <ContentContainer>
                    <View style={{ paddingHorizontal: 20, display: 'flex', width: '100%', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: '20%' }}>
                            <OrButton><OrButtonText>Or</OrButtonText></OrButton>
                        </View>
                        <View style={{ width: '80%' }}>
                            <Heading>Choose Distance From Current Location</Heading>
                        </View>
                    </View>
                </ContentContainer>

                <ContentContainer>
                    <Slider
                        style={{ width: windowWidth - 20, height: 40 }}
                        thumbTintColor={'#059dab'}
                        thumbImage={require('./thumb.png')}
                        onValueChange={setDistance}
                        value={distance}
                        minimumValue={0}
                        step={1}
                        maximumValue={500}
                        minimumTrackTintColor="#000000"
                        maximumTrackTintColor="#000000"
                    />
                </ContentContainer>

                <Text style={{
                    ...styles.pageHeadings,
                    fontSize: 26,
                    color: '#439baa',
                    marginLeft: 5,
                    marginTop: 5,
                    marginBottom: 20
                }}>{distance} KM</Text>

                <Heading>Maximum Price</Heading>
                <ContentContainer>
                    <Slider
                        style={{ width: windowWidth - 20, height: 40 }}
                        thumbTintColor={'#059dab'}
                        thumbImage={require('./thumb.png')}
                        onValueChange={setPrice}
                        value={price}
                        minimumValue={0}
                        step={1}
                        maximumValue={10000}
                        minimumTrackTintColor="#000000"
                        maximumTrackTintColor="#000000"
                    />
                </ContentContainer>

                <Text style={{
                    ...styles.pageHeadings,
                    fontSize: 26,
                    color: '#439baa',
                    marginLeft: 5
                }}>R {price}</Text>

                <Button loading={isLoadingAllDeals} onPress={_searchPriceLocationDeals} title="Search Deals" buttonStyle={{ backgroundColor: '#39464f', paddingHorizontal: 20, paddingVertical: 10, borderRadius: 40, minWidth: 240, marginTop: 30 }} />

            </>);
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    pageHeadings: {
        fontSize: 20,
        fontFamily: 'Rubik-Black',
        color: '#5c5c62'
    },
    header: {
        height: 80,
        backgroundColor: '#439baa',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontSize: 24,
        fontFamily: 'Rubik-Black'
    },
    subHeaderText: {
        color: 'white',
        fontSize: 14,
        fontFamily: 'Rubik-Regular'
    },
    cardHeader: {
        flex: 1,
        color: '#439baa',
        fontSize: 22,
        marginLeft: 5,
        alignSelf: 'flex-start',
        marginBottom: 10,
        fontFamily: 'Rubik-Black'
    },
    modalHeader: {
        color: '#fff',
        fontSize: 22,
        marginBottom: 20,
        fontFamily: 'Rubik-Black'
    },
    modalText: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { height: -3 },
                shadowOpacity: 0.1,
                shadowRadius: 3,
            },
            android: {
                elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        fontFamily: 'Rubik-Regular',
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});

export default SearchByLocation;