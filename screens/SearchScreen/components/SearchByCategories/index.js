import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, View, Text, Dimensions, FlatList, ActivityIndicator, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Input } from 'react-native-elements'
import SelectMultiple from '../../../../components/MultiSelect';
import Heading from '../Heading';
import ContentContainer from '../ContentContainer';
import { getAllCategories, searchCategoryDeals } from '../../../../redux/deals/actions';
import DealListItem from '../../../../components/DealListItem';

const renderLabel = (label, style) => {
    return (
        <View style={{ flexDirection: 'column', alignItems: 'center' }}>
            <View >
                <Text style={style}>{label}</Text>
            </View>
        </View>
    )
}

const SearchByCategories = ({ navigation }) => {

    const windowWidth = Dimensions.get('window').width;

    const dispatch = useDispatch()
    const [deals, setDeals] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);
    const [mappedCategories, setMappedCategories] = useState([]);

    const categories = useSelector(s => s.deals.categories.data)
    const globalDeals = useSelector(s => s.deals.search_deals.data)
    const isLoadingAllDeals = useSelector(s => s.deals.search_deals.isLoading)
    const isRefreshingAllDeals = useSelector(s => s.deals.search_deals.isRefreshing)


    const _searchCategoriesDeals = useCallback(() => {
        dispatch(searchCategoryDeals(selectedItems.flatMap(item => item.value)))
    }, [selectedItems])

    useEffect(() => {
        if (categories && categories.length && !mappedCategories.length) {
            setMappedCategories(categories.map((item) => {
                return {
                    label: item.name,
                    value: item.id
                }
            }))
        }
    }, [categories])

    useEffect(() => {
        if (globalDeals && globalDeals.length) {
            setDeals(globalDeals)
        }
    }, [globalDeals])

    const _renderFooter = () => {
        if (isLoadingAllDeals && !isRefreshingAllDeals) {
            return (
                <View style={styles.tabBarInfoContainer}>
                    <ActivityIndicator animating size="large" />
                </View>
            );
        }
        return null;
    };

    const _onSelectedItemsChange = (selections, item) => {
        setSelectedItems(selections);
    }

    useEffect(() => {
        dispatch(getAllCategories())
    }, [])

    return (
        deals && deals.length && deals.length > 0 ? <View style={{ width: '100%' }}><FlatList
            data={deals}
            refreshing={isRefreshingAllDeals}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (<View style={{ marginBottom: 10 }}><DealListItem navigation={navigation} deal={item} /></View>)}
            ListFooterComponent={_renderFooter}
        />
            <View style={{ position: 'absolute', bottom: 10, right: 10 }}><Button onPress={() => setDeals([])} buttonStyle={{ width: 120, height: 45, backgroundColor: '#39464f', display: 'flex', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }} title={'Back'} titleStyle={{ color: '#FFFFFF', fontFamily: 'Baloo', fontSize: 18 }} /></View>
        </View>
            : <>
                <ContentContainer>
                    <Heading style={{ marginHorizontal: 10 }}>Search Categories</Heading>
                    <Input
                        placeholde="Search Categories"
                        placeholderTextColor="#cccccc"
                        onChangeText={(text) => {
                            setMappedCategories(categories.map((item) => {
                                return {
                                    label: item.name,
                                    value: item.id
                                }
                            }).filter(item => item.label.toLowerCase().includes(text.toLocaleLowerCase())))
                        }}
                        inputContainerStyle={{
                            width: windowWidth - 60,
                            borderColor: '#ccc',
                            borderWidth: 1,
                            borderRadius: 10,
                            padding: 0,
                            margin: 0
                        }}
                        inputStyle={{
                            padding: 10,
                            margin: 0
                        }}
                    />
                </ContentContainer>
                <View style={{
                    width: windowWidth,
                    height: 350
                }}>
                    <SelectMultiple
                        items={mappedCategories}
                        renderLabel={renderLabel}
                        selectedItems={selectedItems}
                        checkboxSource={require('./select.png')}
                        selectedCheckboxSource={require('./selected.png')}
                        onSelectionsChange={_onSelectedItemsChange}
                        rowStyle={{
                            flexDirection: 'column-reverse',
                            paddingTop: 20,
                            paddingBottom: 28
                        }}
                        checkboxStyle={{
                            width: 150,
                            height: 44,
                            marginTop: 8
                        }}
                        labelStyle={{
                            color: '#439baa',
                            fontFamily: 'Baloo',
                            fontSize: 22
                        }}
                    />
                </View>

                <Button loading={isLoadingAllDeals} onPress={_searchCategoriesDeals} title="Search Deals" buttonStyle={{ backgroundColor: '#39464f', paddingHorizontal: 20, paddingVertical: 10, borderRadius: 8, minWidth: 240, marginTop: 30 }} />
            </>);
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    pageHeadings: {
        fontSize: 20,
        fontFamily: 'Rubik-Black',
        color: '#5c5c62'
    },
    header: {
        height: 80,
        backgroundColor: '#439baa',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerText: {
        color: 'white',
        fontSize: 24,
        fontFamily: 'Rubik-Black'
    },
    subHeaderText: {
        color: 'white',
        fontSize: 14,
        fontFamily: 'Rubik-Regular'
    },
    cardHeader: {
        flex: 1,
        color: '#439baa',
        fontSize: 22,
        marginLeft: 5,
        alignSelf: 'flex-start',
        marginBottom: 10,
        fontFamily: 'Rubik-Black'
    },
    modalHeader: {
        color: '#fff',
        fontSize: 22,
        marginBottom: 20,
        fontFamily: 'Rubik-Black'
    },
    modalText: {
        color: '#fff',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    developmentModeText: {
        marginBottom: 20,
        color: 'rgba(0,0,0,0.4)',
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        color: 'rgba(96,100,109, 1)',
        lineHeight: 24,
        textAlign: 'center',
    },
    tabBarInfoContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { height: -3 },
                shadowOpacity: 0.1,
                shadowRadius: 3,
            },
            android: {
                elevation: 20,
            },
        }),
        alignItems: 'center',
        backgroundColor: '#fbfbfb',
        paddingVertical: 20,
    },
    tabBarInfoText: {
        fontSize: 17,
        fontFamily: 'Rubik-Regular',
        color: 'rgba(96,100,109, 1)',
        textAlign: 'center',
    },
    navigationFilename: {
        marginTop: 5,
    },
    helpContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        fontSize: 14,
        color: '#2e78b7',
    },
});

export default SearchByCategories;