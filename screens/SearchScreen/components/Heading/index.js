import styled from '@emotion/native'

const Heading = styled.Text`
    font-family: 'Baloo';
    color: #1e4d50;
    flex-wrap: wrap;
    line-height: 24px;
    padding-top: 20px;
    font-size: 24px;
`

export default Heading;