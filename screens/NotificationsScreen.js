import React, { useEffect } from 'react';
import {
  FlatList,
  StyleSheet,
  View
} from 'react-native';
import { Icon, ListItem, Text } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";
import theme from '../configs/theme';
import { getAllNotifications, removeNotificationReset, removeNotification } from '../redux/auth/actions';

const NotificationsScreen = (props) => {
  const dispatch = useDispatch()

  const notifications = useSelector(s => s.auth.notifications.data)
  const removeSuccess = useSelector(s => s.auth.notifications.removeData)
  const loading = useSelector(s => s.auth.notifications.isLoading)

  useEffect(() => {
    dispatch(getAllNotifications())
  }, [])

  useEffect(() => {
    if (removeSuccess) {
      dispatch(removeNotificationReset())
      dispatch(getAllNotifications())
    }
  }, [removeSuccess])

  const _navigateToDeal = (deal) => {
    props.navigation.navigate('Deal', { deal: deal });
  }

  const _handleRefresh = () => {
    dispatch(getAllNotifications())
  };

  const _keyExtractor = (item) => item.id.toString()


  const renderItem = ({ item }) => (
    <ListItem
      style={{ borderBottomEndRadius: 1, borderBottomColor: theme.colors.textMedium }}
      onPress={() => _navigateToDeal(item.deal)}
      bottomDivider
    >
      <Icon type='ionicon' name='ios-notifications-outline' color={theme.colors.textMedium} />
      <ListItem.Content>
        <ListItem.Title style={{ color: theme.colors.primary }}>{`There's a Deal in your vicinity!`}</ListItem.Title>
        <ListItem.Subtitle style={{ color: theme.colors.textMedium }}>{`The deal name is "${item.deal.name}" and is available at ${item.deal.store.name}.`}</ListItem.Subtitle>
      </ListItem.Content>
      <Icon type='ionicon' onPress={() => { dispatch(removeNotification(item.id)) }} name='ios-close-circle' color={theme.colors.primary} />
    </ListItem>
  );

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Notifications</Text>
        <Text style={styles.subHeaderText}>{notifications && (notifications.length + ' Notifications')}</Text>
      </View>
      <FlatList
        onRefresh={_handleRefresh}
        refreshing={loading}
        keyExtractor={_keyExtractor}
        {...(notifications.length < 1 && { ListHeaderComponent: <Text style={styles.noNotificationsText}>No Current Notifications. Pull to refresh.</Text> })}
        data={notifications}
        renderItem={renderItem}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.textLight,
  },
  header: {
    height: 80,
    backgroundColor: theme.colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerText: {
    color: 'white',
    fontSize: 24,
    fontFamily: 'Rubik-Black'
  },
  subHeaderText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'Rubik-Regular'
  },
  noNotificationsText: {
    alignSelf: 'center',
    marginTop: 40,
    color: theme.colors.textMedium
  }
});

export default NotificationsScreen;