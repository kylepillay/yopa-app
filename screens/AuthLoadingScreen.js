import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import React, { useEffect, useState, useRef } from 'react';
import {
  ActivityIndicator,
  Text,
  View,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import theme from '../configs/theme';
import axios from 'axios'

import config from "../constants/Environment";
import { setUserData } from '../redux/auth/actions';
import * as locationActions from '../redux/location/actions';
import authService from '../services/authService';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const AuthLoadingScreen = (props) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user.data)
  const controller = new AbortController();

  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    if (user && user.id) {
      props.navigation.navigate('Main');
    } else {
      authService._getAccessToken().then((access_token) => {
        if (!access_token) {
          props.navigation.navigate('Auth');
        }
      })
    }
  }, [user, props.navigation])

  useEffect(() => {
    let isSubscribed = true
    authService._getAccessToken().then((access_token) => {
      if (access_token && isSubscribed) {
        authService.setSession(access_token).then(() => {
          authService.getUserData()
            .then((userData) => {
              dispatch(setUserData(userData))
            }).catch(error => {
              authService._removeAccessToken()
              props.navigation.navigate('Auth')
            })
        })
      } else {
        props.navigation.navigate('Auth');
      }
    })
    return () => isSubscribed = false
  }, [])

  useEffect(() => {
    if (expoPushToken) {
      const URL = config.serverUrl + `/api/mobile/notifications/push/register`;

      axios
        .request({
          url: URL,
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            token: expoPushToken.data
          },
          signal: controller.signal
        })
        .then(response => {
          console.log('Push Registered')
        })
        .catch(error => {
          console.log(error)
        });
    }

    return () => {
      controller.abort()
    }
  }, [expoPushToken])

  const mounted = useRef(false);

  useEffect(() => {
    mounted.current = true;
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      if (mounted.current) {
        setNotification(notification);
      }
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log("Added notification response received listener");
    });

    dispatch(locationActions.getLocation())

    return () => {
      mounted.current = false
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, [])

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text style={{ color: theme.colors.textMedium, marginBottom: 10 }}>Authenticating...</Text>
      <ActivityIndicator style={{ alignSelf: 'center' }} />
    </View>
  );
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Device.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}

export default AuthLoadingScreen;