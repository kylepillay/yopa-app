import React, { useRef, useEffect } from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, View } from 'react-native';
import {
  Button,
  Icon
} from 'react-native-elements'
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from "react-redux";

import Input from '../components/AuthInput'
import { submitRegister } from '../redux/auth/actions';

const RegisterScreen = (props) => {
  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const { register, setValue, handleSubmit, errors, watch } = useForm()

  const nameInput = useRef()
  const emailInput = useRef()
  const passwordInput = useRef()
  const passwordValue = useRef()
  const passwordConfirmInput = useRef()

  passwordValue.current = watch('password')

  useEffect(() => {
    if (auth.register.success) {
      props.navigation.goBack();
    }
  }, [auth.register])

  useEffect(() => {
    register('name', { required: "Name is required" })
    register('email', { required: "Email is required" })
    register('password', { required: "Password is required" })
    register('password_confirmation', {
      validate: value =>
        value === passwordValue.current || "The passwords do not match"
    })
  }, [register])

  const onSubmit = (values) => {
    console.log(values)
    Keyboard.dismiss();
    dispatch(submitRegister(values));
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : 'height'} enabled>
      <View>
        <Input
          onChangeText={text => setValue('name', text)}
          placeholder='Name'
          name='name'
          returnKeyType='next'
          ref={nameInput}
          errorMessage={errors['name'] && errors['name'].message}
          onSubmitEditing={() => { emailInput.current.focus() }}
          iconName='ios-person'
        />
        <Input
          onChangeText={text => setValue('email', text)}
          placeholder='Email Address'
          keyboardType='email-address'
          name='email'
          autoCapitalize='none'
          returnKeyType='next'
          onSubmitEditing={() => passwordInput.current.focus()}
          ref={emailInput}
          errorMessage={errors['email'] && errors['email'].message}
          containerStyle={{ marginVertical: 15 }}
          iconName='ios-mail'
        />
        <Input
          onChangeText={text => setValue('password', text)}
          placeholder='Password'
          returnKeyType='next'
          name='password'
          secureTextEntry
          onSubmitEditing={() => passwordConfirmInput.current.focus()}
          errorMessage={errors['password'] && errors['password'].message}
          ref={passwordInput}
          iconName='lock-closed'
        />
        <Input
          onChangeText={text => setValue('password_confirmation', text)}
          placeholder='Confirm Password'
          name='password_confirmation'
          secureTextEntry
          returnKeyType='go'
          ref={passwordConfirmInput}
          errorMessage={errors['password_confirmation'] && errors['password_confirmation'].message}
          onSubmitEditing={handleSubmit(onSubmit)}
          iconName='lock-closed'
        />
        <Button
          onPress={handleSubmit(onSubmit)}
          loading={auth.register.loading}
          loadingProps={{ color: '#439baa' }}
          type="outline"
          titleStyle={styles.buttonTitle}
          containerStyle={styles.button}
          raised
          title='REGISTER'
          icon={<Icon name="ios-person-add" color='#439baa' type='ionicon' size={26}
            containerStyle={{ marginRight: 15 }} />}
        />
      </View>
      <Button
        type="clear"
        titleStyle={{ color: '#fff' }}
        containerStyle={styles.button}
        onPress={() => { props.navigation.goBack() }}
        title='Back to Sign In'
      />
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 40,
    backgroundColor: '#439baa',
    flex: 1,
    flexDirection: 'column'
  },
  buttonText: {
    color: 'white'
  },
  buttonTitle: {
    color: '#439baa',
    marginBottom: 2
  },
  textLink: {
    marginTop: 20,
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    color: '#fff'
  },
  button: {
    marginTop: 15
  }
});


export default RegisterScreen;
