import React, { useRef, useEffect } from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, View, Image } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from "react-redux";

import Logo from '../assets/images/yopa-logo-white.png';
import Input from '../components/AuthInput'
import { submitForgot } from '../redux/auth/actions';

const ForgotScreen = (props) => {
  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const { register, setValue, handleSubmit, errors } = useForm()

  const emailInput = useRef()

  useEffect(() => {
    if (auth.forgot.success) {
      props.navigation.goBack();
    }
  }, [auth.forgot])

  useEffect(() => {
    register('email', { required: "Email is required" })
  }, [register])

  const onSubmit = (values) => {
    Keyboard.dismiss();
    dispatch(submitForgot(values));
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
      <Image
        source={Logo}
        height={24}
        width={100}
        style={{ marginBottom: 100 }}
      />
      <View style={{ width: '100%' }}>
        <Input
          onChangeText={text => setValue('email', text)}
          name='email'
          ref={emailInput}
          placeholder='Email Address'
          keyboardType='email-address'
          errorMessage={errors['email'] && errors['email'].message}
          autoCapitalize='none'
          returnKeyType='go'
          textContentType='emailAddress'
          iconName='ios-mail'
        />
        <Button
          onPress={handleSubmit(onSubmit)}
          loading={auth.forgot.loading}
          loadingProps={{ color: '#439baa' }}
          type="outline"
          titleStyle={{ color: '#439baa', marginBottom: 2 }}
          containerStyle={styles.button}
          raised
          title='RESET PASSWORD'
          icon={<Icon name="ios-exit" color='#439baa' type='ionicon' size={26} containerStyle={{ marginRight: 15 }} />}
        />
      </View>
      <Button
        type="clear"
        titleStyle={{ color: '#fff' }}
        containerStyle={styles.button}
        onPress={() => { props.navigation.goBack() }}
        title='Back to Login'
      />
    </KeyboardAvoidingView>
  );

}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: '#439baa',
    flex: 1,
    flexDirection: 'column'
  },
  buttonText: {
    color: 'white'
  },
  textLink: {
    marginTop: 20,
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    color: '#fff'
  },
  button: {
    marginTop: 15
  },
  errorStyle: {
    fontFamily: 'Rubik-Regular',
    color: '#ab2130'
  }
});


export default ForgotScreen;
