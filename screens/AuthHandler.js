import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

const AuthHandler = (props) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user.data)

  useEffect(() => {
    if (user && user.id) {
      props.navigation.navigate('Main');
    }
  }, [user, props.navigation])

  return (
    <>
      {props.children}
    </>
  );
}

export default AuthHandler;