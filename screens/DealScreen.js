import React, { useCallback, useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  View
} from 'react-native';
import { Badge, Button, Icon, Image, Text } from "react-native-elements";
import { useDispatch, useSelector } from 'react-redux';
import theme from '../configs/theme';

import config from '../constants/Environment';
import { smashDeal, smashDealReset, favouriteDeal, favouriteDealReset, grabDeal, grabDealReset } from '../redux/deals/actions';

const DealScreen = ({ navigation, route }) => {

  const dispatch = useDispatch()

  const favouriteDealState = useSelector(state => state.deals.favourite_deal)
  const smashDealState = useSelector(state => state.deals.smash_deal)
  const grabDealState = useSelector(state => state.deals.grab_deal)

  const [deal, setDeal] = useState({})

  useEffect(() => {
    if (route?.params?.deal) {
      setDeal(route.params.deal)
    }
  }, [route])

  useEffect(() => {
    if (favouriteDealState?.data) {
      dispatch(favouriteDealReset())
      setDeal({ ...deal, favourited: !deal.favourited })
    }
  }, [favouriteDealState, deal])

  useEffect(() => {
    if (smashDealState?.data) {
      console.log('Smash Deal Successful')
      dispatch(smashDealReset())
      navigation.goBack()
    }
  }, [smashDealState])

  useEffect(() => {
    if (grabDealState?.data) {
      dispatch(grabDealReset())
    }
  }, [grabDealState])

  const _smashDeal = useCallback(() => {
    dispatch(smashDeal(deal.id))
  }, [deal]);

  const _grabDeal = useCallback(() => {
    dispatch(grabDeal(deal.id))
    navigation.navigate('Grab Deal', { deal });
  }, [navigation, deal]);

  const _favouriteDeal = useCallback(() => {
    dispatch(favouriteDeal(deal))
  }, [deal]);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.listHeaderContainer}>
          <View style={styles.listHeaderTextContainer}>
            <Text style={styles.detailsHeader}>{deal.name}</Text>
          </View>
          <View style={styles.listSubheaderTextContainer}>
            <Text style={styles.listSubheaderText}>Price:
              R {deal.price} / Sale Price: R {deal.salePrice}</Text>
          </View>
          <Image
            source={{
              uri: `${deal.imageurl}`
            }}
            style={styles.mainImage}
          />
          <Badge
            status="primary"
            textStyle={styles.imageBadgeText}
            value={'You Save: R ' + (deal.price - deal.salePrice).toFixed(2)}
            containerStyle={styles.imageBadgeContainer}
            badgeStyle={styles.imageBadge}
          />

        </View>
        <View style={styles.header}>
          <Button
            onPress={_grabDeal}
            loadingProps={styles.buttonLoadingProps}
            titleStyle={styles.buttonTitle}
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            color={theme.colors.primary}
            raised
            title='GRAB THIS DEAL'
            icon={<Icon name="ios-cart" color={theme.colors.textLight} type='ionicon' size={26} containerStyle={styles.buttonIcon} />}
          />
          <Button
            onPress={_smashDeal}
            loading={smashDealState.isLoading}
            loadingProps={styles.buttonLoadingProps}
            titleStyle={styles.buttonTitle}
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            color={theme.colors.primary}
            raised
            title='SMASH THIS DEAL'
            icon={<Icon name="ios-trash" color={theme.colors.textLight} type='ionicon' size={26} containerStyle={{ marginRight: 20 }} />}
          />
          <Button
            onPress={_favouriteDeal}
            loading={favouriteDealState.isLoading}
            loadingProps={styles.buttonLoadingProps}
            titleStyle={styles.buttonTitle}
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            color={theme.colors.primary}
            raised
            title={deal.favourited ? 'REMOVE FROM FAVOURITES' : 'ADD TO FAVOURITES'}
            icon={<Icon name="ios-heart" color={theme.colors.textLight} type='ionicon' size={26} containerStyle={{ marginRight: 20 }} />}
          />
        </View>
        <View style={styles.detailsHeaderContainer}>
          <Text style={styles.detailsHeader}>Details</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{deal.description}</Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
  },
  topBarLeftIcon: {
    flex: 1,
    alignSelf: 'flex-start',
    marginLeft: 5
  },
  topBarLeftIconContainer: {
    width: 40,
    backgroundColor: theme.colors.primary
  },
  topBarCenterText: {
    color: theme.colors.textLight
  },
  header: {
    height: 165,
    marginBottom: 25,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listHeaderContainer: {
    marginVertical: 10,
    width: '100%'
  },
  listHeaderTextContainer: {
    height: 50,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  detailsHeaderContainer: {
    height: 40,
    borderColor: theme.colors.primary,
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1
  },
  button: {
    borderRadius: 10,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    backgroundColor: theme.colors.primary
  },
  buttonIcon: {
    marginRight: 20
  },
  buttonLoadingProps: {
    color: theme.colors.textLight
  },
  buttonTitle: {
    color: theme.colors.textLight,
    marginBottom: 2
  },
  buttonContainer: {
    width: '100%',
    marginHorizontal: 10,
    marginTop: 15,
    borderRadius: 10
  },
  descriptionContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: theme.colors.textLight,
    alignItems: 'center',
    borderColor: theme.colors.textLight,
    borderTopWidth: 1,
    borderBottomWidth: 1
  },
  mainImage: {
    height: 300,
    marginTop: 10,
    marginHorizontal: 10
  },
  imageBadge: {
    backgroundColor: theme.colors.primary,
    height: 50,
    paddingHorizontal: 10
  },
  imageBadgeText: {
    fontFamily: 'Baloo',
    color: theme.colors.textLight,
    fontSize: 20
  },
  imageBadgeContainer: {
    position: 'absolute',
    top: 100,
    right: 20
  },
  detailsHeader: {
    color: theme.colors.primary,
    fontSize: 26,
    marginLeft: 10,
    fontFamily: 'Rubik-Medium'
  },
  listSubheaderText: {
    fontFamily: 'Baloo',
    color: theme.colors.textMedium,
    fontSize: 18,
    marginLeft: 10
  },
  listSubheaderTextContainer: {
    height: 30,
    paddingHorizontal: 10,
    justifyContent: 'center'
  },
  description: {
    color: theme.colors.textMedium,
    fontSize: 18,
    flexWrap: 'wrap',
    fontFamily: 'Rubik-Regular',
    marginHorizontal: 20,
    marginVertical: 20
  }
});

export default DealScreen;
