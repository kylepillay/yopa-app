import _ from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  View
} from 'react-native';
import { Header, Icon, Text, ButtonGroup } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";

import { getAllDeals, getAllCategories } from '../redux/deals/actions';
import { checkNotifications } from '../redux/auth/actions'
import { getAllFavourites } from '../redux/auth/actions'
import DealListItem from '../components/DealListItem'

const HomeScreen = ({ navigation }) => {
  const buttons = ['All Deals', 'My Deals', 'Stuff I Want'];

  const dispatch = useDispatch()
  const [loading, setLoading] = useState([])
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [deals, setDeals] = useState([])
  const [favouritedDeals, setFavouritedDeals] = useState([])

  const isLoadingAllFavourites = useSelector(s => s.auth.favourites.isLoading)
  const favourites = useSelector(s => s.auth.favourites.data)

  const globalDeals =  useSelector(s => s.deals.all_deals.data)
  const isLoadingAllDeals =  useSelector(s => s.deals.all_deals.isLoading)
  const isRefreshingAllDeals =  useSelector(s => s.deals.all_deals.isRefreshing)
  
  const location = useSelector(s => s.location.location.coords)


  useEffect(() => {
    dispatch(getAllDeals(1));
    dispatch(getAllFavourites(1));
    dispatch(getAllCategories(1));
  }, [selectedIndex])

  useEffect(() => {
    if (isLoadingAllDeals | isLoadingAllFavourites) {
      setLoading(true);
    } else {
      setLoading(false);
    }
  }, [isLoadingAllDeals, isLoadingAllFavourites])

  useEffect(() => {
    if (location) {
      setInterval(() => {
        dispatch(checkNotifications(location.latitude, location.longitude));
      }, 5000);
    }
  }, [])

  useEffect(() => {
    if(favourites) {
      setFavouritedDeals(favourites)
    }
  }, [favourites])

  useEffect(() => {
    if(globalDeals) {
      if(selectedIndex === 0) {
        setDeals(_.filter(globalDeals, { 'smashed': false }))
      } else if (selectedIndex === 1) {
        setDeals(_.filter(globalDeals, { 'grabbed': true }))
      } else {
        setDeals(favouritedDeals)
      }
    }
  }, [globalDeals, favouritedDeals, selectedIndex])

  const _updateIndex = useCallback((newIndex) => {
    setSelectedIndex(newIndex)

  }, []);

  const _handleLoadMore = useCallback(() => {
    // Handle Loading More
  }, []);

  const _handleRefresh = useCallback(() => {
    dispatch(getAllDeals(1));
  }, []);

  const _renderFooter = () => {
    if (loading && !isRefreshingAllDeals) {
      return (
        <View style={styles.tabBarInfoContainer}>
          <ActivityIndicator animating size="large" />
        </View>
      );
    }
    return null;
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>All Deals</Text>
        <Text style={styles.subHeaderText}>{deals.length + ' Products'}</Text>
        <Icon
          raised
          name='ios-options'
          type='ionicon'
          size={18}
          onPress={() => { navigation.navigate('Search') }}
          containerStyle={styles.headerRightIcon}
          color='#439baa'
        />
      </View>
      <ButtonGroup
          onPress={_updateIndex}
          selectedIndex={selectedIndex}
          selectedButtonStyle={{ backgroundColor: '#439baa' }}
          buttons={buttons}
          containerStyle={{ height: 60, margin: 0, padding: 0 }}
        />
      {
        deals && deals.length && deals.length > 0 ? <FlatList
        onEndReached={_handleLoadMore}
        data={deals}
        onRefresh={_handleRefresh}
        refreshing={isRefreshingAllDeals}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (<DealListItem navigation={navigation} deal={item} />)}
        ListFooterComponent={_renderFooter}
      />
       : null }
      {loading && <ActivityIndicator style={styles.loadingActivityIndicator} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    height: 80,
    backgroundColor: '#439baa',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerText: {
    color: 'white',
    fontSize: 24,
    fontFamily: 'Rubik-Black'
  },
  subHeaderText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'Rubik-Regular'
  },
  headerRightIcon: {
    position: 'absolute',
    right: 5
  },
  loadingActivityIndicator: {
    alignSelf: 'center',
    marginTop: 40
  }
});

HomeScreen['navigationOptions'] = ({ navigation }) => ({
  header: () => <Header
        backgroundColor='#439baa'
        centerComponent={{ text: 'Home', style: { color: '#fff' } }}
        rightComponent={<Icon
          type='ionicon'
          name='ios-heart-empty'
          onPress={() => { navigation.navigate('Favourites') }}
          style={{ flex: 1, alignSelf: 'flex-end' }}
          color='#ffffff'
        />}
      />,
})

export default HomeScreen;