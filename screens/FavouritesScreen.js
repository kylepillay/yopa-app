import React, { useEffect } from 'react';
import {
  FlatList,
  StyleSheet,
  View
} from 'react-native';
import { Text } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";

import DealListItem from "../components/DealListItem";
import theme from '../configs/theme';
import { getAllFavourites } from '../redux/auth/actions';
import { favouriteDealReset, updateDealFavourite, favouriteDeal } from '../redux/deals/actions';

const FavouritesScreen = (props) => {
  const dispatch = useDispatch()

  const favouriteDealSuccess = useSelector(s => s.deals.favourite_deal.data)
  const favouriteDealLoading = useSelector(s => s.deals.favourite_deal.isLoading)
  const favouriteDealError = useSelector(s => s.deals.favourite_deal.error)
  const loading = useSelector(s => s.auth.favourites.isLoading)
  const favourites = useSelector(s => s.auth.favourites.data)

  useEffect(() => {
    dispatch(getAllFavourites())
  }, [])

  useEffect(() => {
    if (favouriteDealSuccess) {
      dispatch(updateDealFavourite(favouriteDealSuccess.deal))
      dispatch(favouriteDealReset());
      dispatch(getAllFavourites())
    }
  }, [favouriteDealSuccess])

  const _keyExtractor = (item) => item.id.toString();

  const _favouriteDeal = (deal) => {
    dispatch(favouriteDeal(deal))
  };

  const _handleRefresh = () => {
    dispatch(getAllFavourites())
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>My Favourites</Text>
        <Text style={styles.subHeaderText}>{favourites && (favourites.length + ' Favourites')}</Text>
      </View>
      <FlatList
        data={favourites}
        onRefresh={_handleRefresh}
        refreshing={loading}
        keyExtractor={_keyExtractor}
        {...(favourites.length < 1 && { ListHeaderComponent: <Text style={styles.noFavouritesText}>No Favourites. Pull to Refresh.</Text> })}
        renderItem={({ item }) => (<DealListItem navigation={props.navigation} onPressFavourite={() => _favouriteDeal(item)} favouriteDealLoading={favouriteDealLoading} deal={item} />)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.textLight,
  },
  header: {
    height: 80,
    backgroundColor: theme.colors.primary,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerText: {
    color: 'white',
    fontSize: 24,
    fontFamily: 'Rubik-Black'
  },
  subHeaderText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'Rubik-Regular'
  },
  noFavouritesText: {
    alignSelf: 'center',
    marginTop: 40,
    color: theme.colors.textMedium
  }
});

export default FavouritesScreen;