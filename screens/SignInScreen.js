import React, { useEffect, useRef } from 'react';
import { Keyboard, KeyboardAvoidingView, StyleSheet, View, Image, TouchableWithoutFeedback } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import * as Device from 'expo-device';
import { submitLogin } from '../redux/auth/actions';
import Logo from '../assets/images/yopa-logo-white.png';
import { useDispatch, useSelector } from "react-redux";
import { useForm } from 'react-hook-form';

import Input from '../components/AuthInput'
import theme from '../configs/theme';

const SignInScreen = (props) => {
  const auth = useSelector(state => state.auth)
  const user = useSelector(state => state.auth.user.data)
  const dispatch = useDispatch()
  const { register, setValue, handleSubmit, errors } = useForm()

  const emailInput = useRef()
  const passwordInput = useRef()

  useEffect(() => {
    register('email', { required: "Email is required" })
    register('password', { required: "Password is required" })
  }, [register])

  useEffect(() => {
    if (user && user.id) {
      props.navigation.navigate('Main');
    }
  }, [user, props.navigation])

  const onSubmit = (values) => {
    Keyboard.dismiss();
    dispatch(submitLogin({ ...values, device_name: Device.modelName }));
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : 'height'} enabled>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
      <Image
        source={Logo}
        height={24}
        width={100}
        style={{ marginBottom: 100 }}
      />
      <View style={{ width: '100%' }}>
        <Input
          onChangeText={text => setValue('email', text)}
          name='email'
          placeholder='Email Address'
          keyboardType='email-address'
          autoCapitalize='none'
          returnKeyType='next'
          onSubmitEditing={() => passwordInput.current.focus()}
          errorMessage={errors['email'] && errors['email'].message}
          blurOnSubmit={false}
          ref={emailInput}
          textContentType='emailAddress'
          iconName='ios-mail'
        />
        <Input
          onChangeText={text => setValue('password', text)}
          name='password'
          placeholder='Password'
          secureTextEntry
          returnKeyType='go'
          errorMessage={errors['password'] && errors['password'].message}
          onSubmitEditing={handleSubmit(onSubmit)}
          ref={passwordInput}
          textContentType='password'
          iconName='lock-closed'
        />
        <Button
          onPress={handleSubmit(onSubmit)}
          loading={auth.login.isLoading}
          loadingProps={{ color: theme.colors.primary }}
          type="outline"
          titleStyle={styles.buttonTitle}
          containerStyle={styles.button}
          raised
          title='SIGN IN'
          icon={<Icon name="ios-exit" color={theme.colors.primary} type='ionicon' size={26} containerStyle={{ marginRight: 15 }} />}
        />
      </View>

      <Button
        type="clear"
        titleStyle={{ color: theme.colors.textLight }}
        containerStyle={{ marginTop: 20 }}
        onPress={() => { props.navigation.navigate('Register') }}
        title='Create Account'
      />
      </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {

    backgroundColor: theme.colors.primary,
    flex: 1,
  },
  buttonText: {
    color: 'white'
  },
  textLink: {
    marginTop: 20,
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    color: theme.colors.textLight
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: 'space-around',
  },
  buttonTitle: {
    color: theme.colors.primary,
    marginBottom: 2
  },
  button: {
    marginTop: 15
  }
});


export default SignInScreen;
