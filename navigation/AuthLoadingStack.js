import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import AuthLoadingScreen from '../screens/AuthLoadingScreen';

const Stack = createStackNavigator();

export default () => {
  const navOptions = {
    headerShown: false
  }
  return (
    <Stack.Navigator>
      <Stack.Screen name="Authentication Screen" component={AuthLoadingScreen} options={navOptions} />
    </Stack.Navigator>
  )
}