import React from 'react';
import { StyleSheet, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Header, Icon } from "react-native-elements";

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import FavouritesScreen from '../screens/FavouritesScreen';
import ProfileScreen from '../screens/ProfileScreen';
import DealGrabScreen from '../screens/DealGrabScreen';
import DealScreen from '../screens/DealScreen';
import theme from '../configs/theme';


const BottomTabNavigator = createBottomTabNavigator()
const Stack = createStackNavigator()

const StyledHeader = ({ navigation, scene }) => {

  return (
    <Header
      backgroundColor={theme.colors.primary}
      centerComponent={{ text: scene?.route?.name || 'YoPa', style: styles.topBarCenterText }}
      leftComponent={<View style={styles.topBarLeftIconContainer}>
        <Icon
          type='ionicon'
          name='ios-arrow-back'
          onPress={() => {
            navigation.goBack()
          }}
          style={styles.topBarLeftIcon}
          color='#ffffff'
        />
      </View>}
    />
  )
}

const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Deal" component={DealScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Grab Deal" component={DealGrabScreen} options={{ header: StyledHeader }} />
    </Stack.Navigator>
  )
}

const SearchStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Search" component={SearchScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Deal" component={DealScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Grab Deal" component={DealGrabScreen} options={{ header: StyledHeader }} />
    </Stack.Navigator>
  )
}

const NotificationsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Notifications" component={NotificationsScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Deal" component={DealScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Grab Deal" component={DealGrabScreen} options={{ header: StyledHeader }} />
    </Stack.Navigator>
  )
}


const FavouritesStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Favourites" component={FavouritesScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Deal" component={DealScreen} options={{ header: StyledHeader }} />
      <Stack.Screen name="Grab Deal" component={DealGrabScreen} options={{ header: StyledHeader }} />
    </Stack.Navigator>
  )
}

const ProfileStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} />
    </Stack.Navigator>
  )
}

export default () => {
  return (
    <BottomTabNavigator.Navigator
      tabBarOptions={{
        activeTintColor: '#439baa',
      }}>
      <BottomTabNavigator.Screen
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'ios-home' : 'md-home'}
              />
            )
          }
        }}
        name="Home"
        component={HomeStack}
      />
      <BottomTabNavigator.Screen
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'}
              />
            )
          }
        }}
        name="Search"
        component={SearchStack}
      />
      <BottomTabNavigator.Screen
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'ios-notifications' : 'md-notifications'}
              />
            )
          }
        }}
        name="Notifications"
        component={NotificationsStack}
      />
      <BottomTabNavigator.Screen
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'ios-heart' : 'md-heart'}
              />
            )
          }
        }}
        name="Favourites"
        component={FavouritesStack}
      />
      <BottomTabNavigator.Screen
        options={{
          tabBarIcon: ({ focused }) => {
            return (
              <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
              />
            )
          }
        }}
        name="Profile"
        component={ProfileStack}
      />
    </BottomTabNavigator.Navigator>
  )
}

const styles = StyleSheet.create({
  topBarLeftIcon: {
    flex: 1,
    alignSelf: 'flex-start',
    marginLeft: 5
  },
  topBarLeftIconContainer: {
    width: 40,
    backgroundColor: '#439baa'
  },
  topBarCenterText: {
    color: '#fff',
    fontSize: 16
  }
});
