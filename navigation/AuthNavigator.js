import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import SignInScreen from '../screens/SignInScreen';
import RegisterScreen from '../screens/RegisterScreen';
import ForgotScreen from '../screens/ForgotScreen';

const Stack = createStackNavigator();

export default () => {
  const navOptions = {
    headerShown: false
  }
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={SignInScreen} options={navOptions} />
      <Stack.Screen name="Register" component={RegisterScreen} options={navOptions} />
      <Stack.Screen name="Forgot Password" component={ForgotScreen} options={navOptions} />
    </Stack.Navigator>
  )
}