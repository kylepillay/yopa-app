
import React from 'react'
import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';

import AuthStack from './AuthNavigator'
import AuthLoadingStack from './AuthLoadingStack'
import MainBottomTabNavigator from './MainTabNavigator'

const Stack = createStackNavigator();

export default () => {

  const navOptions = {
    headerShown: false
  }

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Loading" component={AuthLoadingStack} options={navOptions} />
        <Stack.Screen name="Auth" component={AuthStack} options={navOptions} />
        <Stack.Screen name="Main" component={MainBottomTabNavigator} options={navOptions} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}