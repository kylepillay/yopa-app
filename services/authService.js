import axios from 'axios';
import jwtDecode from 'jwt-decode';
import AsyncStorage from '@react-native-async-storage/async-storage';
import config from '../constants/Environment'

class authService {

    constructor() {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }

    setInterceptors = () => {
        axios.interceptors.response.use(response => {
            return response;
        }, err => {
            return new Promise((resolve, reject) => {
                if (err && err.response && err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
                    // if you ever get an unauthorized response, logout the user
                    this.emit('onAutoLogout', 'Invalid access_token');
                    this.setSession();
                    resolve()
                }
                throw reject(err);
            });
        });
    };

    createUser = ({ name, email, password, password_confirmation, role }) => {
        return new Promise((resolve, reject) => {
            axios.post(config.serverUrl + '/api/auth/register', {
                name,
                email,
                password,
                password_confirmation,
                role
            }).then(response => {
                resolve(response.data.data)
            }).catch((error) => {
                reject(error.response);
            });
        });
    };

    signInWithEmailAndPassword = (email, password, device_name) => {
        return new Promise((resolve, reject) => {
            axios.post(config.serverUrl + '/api/auth/login', {
                email,
                password,
                device_name
            }).then(response => {
                this.setSession(response.data.data.access_token).then(() => {
                    resolve(response.data.data.user);
                });
            }).catch((error) => {
                reject(error.response);
            });
        });
    };

    getUserData = () => {
        return new Promise((resolve, reject) => {
            axios.get(config.serverUrl + '/api/auth/user')
                .then(response => {
                    resolve(response.data);
                }).catch((error) => {
                    reject(error.response);
                });
        });
    };

    resetPassword = (email) => {
        return new Promise((resolve, reject) => {
            axios.post(config.serverUrl + '/api/auth/password/forgot', {
                email
            }).then(response => {
                resolve(response.data);
            }).catch((error) => {
                reject(error.response);
            });
        });
    };

    updateUserData = (user) => {
        return new Promise((resolve, reject) => {
            axios.post(config.serverUrl + '/api/auth/update', user)
                .then(response => {
                    resolve(response.data.user);
                }).catch((error) => {
                    reject(error.response);
                });
        });
    };

    removeNotification(notificationId) {
        return new Promise((resolve, reject) => {
            axios.request({
                url: config.serverUrl + `/api/mobile/notifications/remove`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                data: {
                    notification_id: notificationId
                }
            }).then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            }).catch(error => {
                reject(error)
            });
        });
    }

    checkNotifications(latitude, longitude) {
        return new Promise((resolve, reject) => {
            axios.request({
                url: config.serverUrl + `/api/mobile/notifications/check`,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                data: {
                    latitude: latitude,
                    longitude: longitude
                }
            }).then((response) => {
                if (response.data) {
                    resolve(response.data);
                }
            }).catch(error => {
                reject(error)
            });
        });
    }

    getAllNotifications = () => {
        return new Promise((resolve, reject) => {
            axios.get(config.serverUrl + `/api/mobile/notifications`)
                .then(response => {
                    if (response.data) {
                        resolve(response.data);
                    } else {
                        reject(response.data.error);
                    }
                });
        });
    };

    getAllFavourites = () => {
        return new Promise((resolve, reject) => {
            axios.get(config.serverUrl + `/api/mobile/favourites`)
                .then(response => {
                    if (response.data) {
                        resolve(response.data);
                    } else {
                        reject(response.data.error);
                    }
                });
        });
    };

    setSession = (access_token = false) => {
        if (access_token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
            return this._setAccessToken(access_token);
        } else {
            delete axios.defaults.headers.common['Authorization']
            return this._removeAccessToken()
        }
    };

    logout = () => {
        this.setSession();
    };

    isAuthTokenValid = access_token => {
        if (!access_token) {
            return false;
        }
        return true;
    };

    _getAccessToken = async () => {
        try {
            return await AsyncStorage.getItem('access_token');
        } catch (error) {
            console.log('Error Retrieving Token')
            console.log(error)
        }
    };

    _removeAccessToken = async () => {
        try {
            return await AsyncStorage.removeItem('access_token');
        } catch (error) {
            console.log('Error Removing Token')
            console.log(error)
        }
    };

    _setAccessToken = async (access_token) => {
        try {
            return await AsyncStorage.setItem('access_token', access_token);
        } catch (error) {
            console.log('Error Saving Token')
            console.log(error)
        }
    }
}

const instance = new authService();

export default instance;
