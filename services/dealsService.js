import axios from 'axios';
import config from '../constants/Environment'
import EventEmitter from '../utils/EventEmitter';

class dealsService extends EventEmitter {

  getAllDeals = (page) => {
    return new Promise((resolve, reject) => {
      axios.get(config.serverUrl + `/api/mobile/deals`)
        .then(response => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response.data.error);
          }
        }).catch(error => {
          console.log(error.response)
        });
    });
  };

  getAllCategories = () => {
    return new Promise((resolve, reject) => {
      axios.get(config.serverUrl + `/api/mobile/deals/categories`)
        .then(response => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response.data.error);
          }
        });
    });
  };

  getAllProvinces = () => {
    return new Promise((resolve, reject) => {
      axios.get(config.serverUrl + `/api/mobile/provinces`)
        .then(response => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response.data.error);
          }
        });
    });
  };

  searchLocationDeals = (latitude, longitude, radius, price, province) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: config.serverUrl + `/api/mobile/search/location`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        data: {
          latitude: latitude,
          longitude: longitude,
          radius: radius,
          price: price,
          province_id: province
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch(error => {
        reject(error);
      });
    });
  };

  searchCategoryDeals = (categories) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: config.serverUrl + `/api/mobile/search/categories`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        data: {
          categories
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch(error => {
        reject(error);
      });
    });
  };

  searchProvinceDeals = (provinceId) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: config.serverUrl + `/api/mobile/search/province`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        data: {
          province_id: provinceId
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch(error => {
        reject(error);
      });
    });
  };

  searchTextDeals = (search_text) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: config.serverUrl + `/api/mobile/search/text`,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        data: {
          search_text: search_text
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch(error => {
        reject(error);
      });
    });
  };

  favouriteDeal = (deal) => {
    return new Promise((resolve, reject) => {
      axios.post(config.serverUrl + `/api/mobile/favourites/create`, { deal_id: deal.id }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch((error) => {
        reject(error);
      });
    });
  };

  smashDeal = (id) => {
    return new Promise((resolve, reject) => {
      axios.post(config.serverUrl + `/api/mobile/deals/smash`, { deal_id: id }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch((error) => {
        reject(error);
      });
    });
  };

  grabDeal = (id) => {
    return new Promise((resolve, reject) => {
      axios.post(config.serverUrl + `/api/mobile/deals/grab`, { deal_id: id }, {
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          reject(response.data.error);
        }
      }).catch((error) => {
        console.log(error)
        reject(error);
      });
    });
  };


}

const instance = new dealsService();

export default instance;
