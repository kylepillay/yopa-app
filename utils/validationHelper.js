import validation from 'validate.js';

export default function validate(fieldName, value) {
  let constraints = {
    email: {
      presence: true,
      format: {
        pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        message: 'Invalid email',
      }
    },
    password: {
      presence: true,
      length: {
        minimum: 4,
        message: 'Invalid Password. Minimum 4 characters.',
      }
    },
    city: {
      presence: true,
      length: {
        minimum: 4,
        message: 'Invalid City. Minimum 4 Characters',
      }
    },
    name: {
      presence: true,
      length: {
        minimum: 3,
        message: 'Invalid name. Minimum 3 characters.',
      }
    },
    confirm_password: {
      presence: true,
      length: {
        minimum: 4,
        message: 'Invalid Password. Minimum 4 characters.',
      }
    },
    phone: {
      presence: true,
      format: {
        pattern: "^[0-9]{10}$",
        message: 'Invalid phone number',
      },
    },
  };

  let formValues = {};
  formValues[fieldName] = value;

  let formFields = {};
  formFields[fieldName] = constraints[fieldName];


  const result = validation(formValues, formFields);

  if (result) {
    return result[fieldName][0]
  }
  return null
}