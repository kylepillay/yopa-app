import AppLoading from 'expo-app-loading';
import * as Notifications from 'expo-notifications'
import * as Font from 'expo-font';
import React from 'react';
import { Alert, Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AsyncStorage } from '@react-native-async-storage/async-storage';
import { Provider } from 'react-redux';

import { Ionicons } from '@expo/vector-icons';

import GlobalModal from './components/GlobalModal'
import AppNavigator from './navigation/AppNavigator';
import store from './redux';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  componentDidMount() {
    this._notificationSubscription = Notifications.addNotificationReceivedListener(this._handleNotification);
  }

  _handleNotification = (notification) => {
    const { request: { content } } = notification
    Alert.alert(content.data.title, 'You have a new notification.');
  };

  async storeItem(key, item) {
    try {
      return await AsyncStorage.setItem(key, JSON.stringify(item));
    } catch (error) {
      console.log(error.message);
    }
  }

  async retrieveItem(key) {
    try {
      const retrievedItem = await AsyncStorage.getItem(key);
      return JSON.parse(retrievedItem);
    } catch (error) {
      console.log(error.message);
    }
  }

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync.bind(this)}
          onError={this._handleLoadingError.bind(this)}
          onFinish={this._handleFinishLoading.bind(this)}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          <Provider store={store}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <AppNavigator />
            <GlobalModal />
          </Provider>
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([


      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'Rubik-Black': require('./assets/fonts/Rubik-Black.ttf'),
        'Rubik-BlackItalic': require('./assets/fonts/Rubik-BlackItalic.ttf'),
        'Rubik-Bold': require('./assets/fonts/Rubik-Bold.ttf'),
        'Rubik-BoldItalic': require('./assets/fonts/Rubik-BoldItalic.ttf'),
        'Rubik-Italic': require('./assets/fonts/Rubik-Italic.ttf'),
        'Rubik-Light': require('./assets/fonts/Rubik-Light.ttf'),
        'Rubik-LightItalic': require('./assets/fonts/Rubik-LightItalic.ttf'),
        'Rubik-Medium': require('./assets/fonts/Rubik-Medium.ttf'),
        'Rubik-MediumItalic': require('./assets/fonts/Rubik-MediumItalic.ttf'),
        'Rubik-Regular': require('./assets/fonts/Rubik-Regular.ttf'),
        'Baloo': require('./assets/fonts/Baloo-Regular.ttf'),
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
