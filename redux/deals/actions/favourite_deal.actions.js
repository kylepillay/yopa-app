import dealsService from '../../../services/dealsService';

export const FAVOURITE_DEAL_ERROR = 'FAVOURITE_DEAL_ERROR';
export const FAVOURITE_DEAL_PENDING = 'FAVOURITE_DEAL_PENDING';
export const FAVOURITE_DEAL_SUCCESS = 'FAVOURITE_DEAL_SUCCESS';
export const FAVOURITE_DEAL_RESET = 'FAVOURITE_DEAL_RESET';

export function favouriteDeal(deal) {
  return (dispatch) => {
    dispatch({
      type: FAVOURITE_DEAL_PENDING,
      payload: true
    });
    return dealsService.favouriteDeal(deal)
      .then((data) => {
          return dispatch({
            type: FAVOURITE_DEAL_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: FAVOURITE_DEAL_ERROR,
          payload: error
        });
      });
  }
}

export function favouriteDealReset() {
  return (dispatch) => {
    return dispatch({
      type: FAVOURITE_DEAL_RESET,
      payload: true
    })
  }
}
