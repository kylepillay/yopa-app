import dealsService from '../../../services/dealsService';

export const SEARCH_LOCATION_DEALS_ERROR = 'SEARCH_LOCATION_DEALS_ERROR';
export const SEARCH_LOCATION_DEALS_PENDING = 'SEARCH_LOCATION_DEALS_PENDING';
export const SEARCH_LOCATION_DEALS_SUCCESS = 'SEARCH_LOCATION_DEALS_SUCCESS';

export const SEARCH_CATEGORY_DEALS_ERROR = 'SEARCH_CATEGORY_DEALS_ERROR';
export const SEARCH_CATEGORY_DEALS_PENDING = 'SEARCH_CATEGORY_DEALS_PENDING';
export const SEARCH_CATEGORY_DEALS_SUCCESS = 'SEARCH_CATEGORY_DEALS_SUCCESS';

export const SEARCH_PROVINCE_DEALS_ERROR = 'SEARCH_PROVINCE_DEALS_ERROR';
export const SEARCH_PROVINCE_DEALS_PENDING = 'SEARCH_PROVINCE_DEALS_PENDING';
export const SEARCH_PROVINCE_DEALS_SUCCESS = 'SEARCH_PROVINCE_DEALS_SUCCESS';

export const SEARCH_TEXT_DEALS_ERROR = 'SEARCH_TEXT_DEALS_ERROR';
export const SEARCH_TEXT_DEALS_PENDING = 'SEARCH_TEXT_DEALS_PENDING';
export const SEARCH_TEXT_DEALS_SUCCESS = 'SEARCH_TEXT_DEALS_SUCCESS';

export const UPDATE_DEAL_FAVOURITE_SEARCH = 'UPDATE_DEAL_FAVOURITE_SEARCH';
export const UPDATE_DEAL_SMASHED_SEARCH = 'UPDATE_DEAL_SMASHED_SEARCH';

export const CLEAR_SEARCH = 'CLEAR_SEARCH';

export function searchLocationDeals(latitude, longitude, radius, price, province) {
  return (dispatch) => {
    dispatch({
      type: SEARCH_LOCATION_DEALS_PENDING,
      payload: true
    });
    return dealsService.searchLocationDeals(latitude, longitude, radius, price, province)
      .then((data) => {
        return dispatch({
          type: SEARCH_LOCATION_DEALS_SUCCESS,
          payload: { data }
        });
      }
      )
      .catch(error => {
        return dispatch({
          type: SEARCH_LOCATION_DEALS_ERROR,
          payload: error
        });
      });
  }
}

export function searchProvinceDeals(province_id) {
  return (dispatch) => {
    dispatch({
      type: SEARCH_PROVINCE_DEALS_PENDING,
      payload: true
    });
    return dealsService.searchCategoryDeals(province_id)
      .then((data) => {
        return dispatch({
          type: SEARCH_PROVINCE_DEALS_SUCCESS,
          payload: { data }
        });
      }
      )
      .catch(error => {
        return dispatch({
          type: SEARCH_PROVINCE_DEALS_ERROR,
          payload: error
        });
      });
  }
}

export function searchCategoryDeals(categories) {
  return (dispatch) => {
    dispatch({
      type: SEARCH_CATEGORY_DEALS_PENDING,
      payload: true
    });
    return dealsService.searchCategoryDeals(categories)
      .then((data) => {
        return dispatch({
          type: SEARCH_CATEGORY_DEALS_SUCCESS,
          payload: { data }
        });
      }
      )
      .catch(error => {
        return dispatch({
          type: SEARCH_CATEGORY_DEALS_ERROR,
          payload: error
        });
      });
  }
}

export function searchTextDeals(search_text) {
  return (dispatch) => {
    dispatch({
      type: SEARCH_TEXT_DEALS_PENDING,
      payload: true
    });
    return dealsService.searchTextDeals(search_text)
      .then((data) => {
        return dispatch({
          type: SEARCH_TEXT_DEALS_SUCCESS,
          payload: { data }
        });
      }
      )
      .catch(error => {
        return dispatch({
          type: SEARCH_TEXT_DEALS_ERROR,
          payload: error
        });
      });
  }
}

export function clearSearch() {
  return (dispatch) => {
    return dispatch({
      type: CLEAR_SEARCH,
      payload: {}
    });
  }
}

export function updateSearchDealFavourite(deal) {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_DEAL_FAVOURITE_SEARCH,
      payload: { id: deal.id, favourited: deal.favourited }
    });
  }
}


export function updateSearchDealSmashed(deal) {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_DEAL_SMASHED_SEARCH,
      payload: { id: deal.id, smashed: true }
    });
  }
}
