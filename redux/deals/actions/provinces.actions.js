import dealsService from '../../../services/dealsService';

export const GET_ALL_PROVINCES_ERROR = 'GET_ALL_PROVINCES_ERROR';
export const GET_ALL_PROVINCES_PENDING = 'GET_ALL_PROVINCES_PENDING';
export const GET_ALL_PROVINCES_SUCCESS = 'GET_ALL_PROVINCES_SUCCESS';

export function getAllProvinces() {
    return (dispatch) => {
        dispatch({
            type: GET_ALL_PROVINCES_PENDING,
            payload: true
        });
        return dealsService.getAllProvinces()
            .then((data) => {
                return dispatch({
                    type: GET_ALL_PROVINCES_SUCCESS,
                    payload: data
                });
            }
            )
            .catch(error => {
                return dispatch({
                    type: GET_ALL_PROVINCES_ERROR,
                    payload: error
                });
            });
    }
}