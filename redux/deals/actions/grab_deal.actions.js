import dealsService from '../../../services/dealsService';

export const GRAB_DEAL_ERROR = 'GRAB_DEAL_ERROR';
export const GRAB_DEAL_PENDING = 'GRAB_DEAL_PENDING';
export const GRAB_DEAL_SUCCESS = 'GRAB_DEAL_SUCCESS';
export const GRAB_DEAL_RESET = 'GRAB_DEAL_RESET';

export function grabDeal(id) {
  return (dispatch) => {
    dispatch({
      type: GRAB_DEAL_PENDING,
      payload: true
    });
    return dealsService.grabDeal(id)
      .then((data) => {
          return dispatch({
            type: GRAB_DEAL_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: GRAB_DEAL_ERROR,
          payload: error
        });
      });
  }
}

export function grabDealReset() {
  return (dispatch) => {
    return dispatch({
      type: GRAB_DEAL_RESET,
      payload: true
    })
  }
}
