import dealsService from '../../../services/dealsService';

export const SMASH_DEAL_ERROR = 'SMASH_DEAL_ERROR';
export const SMASH_DEAL_PENDING = 'SMASH_DEAL_PENDING';
export const SMASH_DEAL_SUCCESS = 'SMASH_DEAL_SUCCESS';
export const SMASH_DEAL_RESET = 'SMASH_DEAL_RESET';

export function smashDeal(id) {
  return (dispatch) => {
    dispatch({
      type: SMASH_DEAL_PENDING,
      payload: true
    });
    return dealsService.smashDeal(id)
      .then((data) => {
          return dispatch({
            type: SMASH_DEAL_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: SMASH_DEAL_ERROR,
          payload: error
        });
      });
  }
}

export function smashDealReset() {
  return (dispatch) => {
    return dispatch({
      type: SMASH_DEAL_RESET,
      payload: true
    })
  }
}
