import dealsService from '../../../services/dealsService';

export const GET_ALL_DEALS_ERROR = 'GET_ALL_DEALS_ERROR';
export const GET_ALL_DEALS_PENDING = 'GET_ALL_DEALS_PENDING';
export const GET_ALL_DEALS_SUCCESS = 'GET_ALL_DEALS_SUCCESS';
export const UPDATE_DEAL_FAVOURITE = 'UPDATE_DEAL_FAVOURITE';
export const UPDATE_DEAL_SMASHED = 'UPDATE_DEAL_SMASHED';
export const UPDATE_DEAL_GRABBED = 'UPDATE_DEAL_GRABBED';

export function getAllDeals(page) {
  return (dispatch) => {
    dispatch({
      type: GET_ALL_DEALS_PENDING,
      payload: true
    });
    return dealsService.getAllDeals(page)
      .then((data) => {
          return dispatch({
            type: GET_ALL_DEALS_SUCCESS,
            payload: {data, page}
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: GET_ALL_DEALS_ERROR,
          payload: error
        });
      });
  }
}

export function updateDealFavourite(deal) {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_DEAL_FAVOURITE,
      payload: {id: deal.id, favourited: deal.favourited}
    });
  }
}


export function updateDealSmashed(deal) {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_DEAL_SMASHED,
      payload: {id: deal.id, smashed: true}
    });
  }
}

export function updateDealGrabbed(id) {
  return (dispatch) => {
    return dispatch({
      type: UPDATE_DEAL_GRABBED,
      payload: {id, grabbed: true}
    });
  }
}