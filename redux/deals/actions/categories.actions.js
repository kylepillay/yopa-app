import dealsService from '../../../services/dealsService';

export const GET_ALL_CATEGORIES_ERROR = 'GET_ALL_CATEGORIES_ERROR';
export const GET_ALL_CATEGORIES_PENDING = 'GET_ALL_CATEGORIES_PENDING';
export const GET_ALL_CATEGORIES_SUCCESS = 'GET_ALL_CATEGORIES_SUCCESS';

export function getAllCategories() {
  return (dispatch) => {
    dispatch({
      type: GET_ALL_CATEGORIES_PENDING,
      payload: true
    });
    return dealsService.getAllCategories()
      .then((data) => {
          return dispatch({
            type: GET_ALL_CATEGORIES_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: GET_ALL_CATEGORIES_ERROR,
          payload: error
        });
      });
  }
}
