import { combineReducers } from 'redux';
import all_deals from './all_deals.reducer';
import favourite_deal from './favourite_deal.reducer';
import smash_deal from './smash_deal.reducer';
import grab_deal from './grab_deal.reducer';
import search_deals from './search_deals.reducer';
import categories from './categories.reducer';
import provinces from './provinces.reducer';

const dealsReducers = combineReducers({
    all_deals,
    favourite_deal,
    smash_deal,
    grab_deal,
    search_deals,
    categories,
    provinces
});

export default dealsReducers;