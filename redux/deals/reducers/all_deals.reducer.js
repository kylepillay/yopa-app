import * as Actions from '../actions';

const initialState = {
    data: [],
    page: 0,
    last_page: 0,
    isLoading: false,
    isRefreshing: false,
    total: '(Loading)',
    error  : false
};

const all_deals = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_ALL_DEALS_SUCCESS:
        {
            return {
                ...initialState,
                data: action.payload.data.deals
            };
        }
        case Actions.GET_ALL_DEALS_PENDING:
        {
            return {
                ...initialState,
                data: state.data,
                isLoading: true,
                isRefreshing: true
            };
        }
        case Actions.GET_ALL_DEALS_ERROR:
        {
            return {
                ...initialState,
                data: state.data,
                error  : action.payload
            };
        }
        case Actions.UPDATE_DEAL_FAVOURITE:
        {
            return {
                ...state,
                data: state.data.map(
                  (item, i) => item.id === action.payload.id ? {...item, favourited: action.payload.favourited}
                    : item
                )
            }
        }
        case Actions.UPDATE_DEAL_SMASHED:
        {
            return {
                ...state,
                data: state.data.map(
                  (item, i) => item.id === action.payload.id ? {...item, smashed: true}
                    : item
                )
            }
        }
        default:
        {
            return state
        }
    }
};

export default all_deals;