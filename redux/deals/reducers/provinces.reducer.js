import * as Actions from '../actions';

const initialState = {
    data: [],
    isLoading: false,
    isRefreshing: false,
    total: '(Loading)',
    error: false
};

const provinces = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_ALL_PROVINCES_SUCCESS:
            {
                return {
                    ...initialState,
                    data: action.payload
                };
            }
        case Actions.GET_ALL_PROVINCES_PENDING:
            {
                return {
                    ...initialState,
                    data: state.data,
                    isLoading: true,
                    isRefreshing: true
                };
            }
        case Actions.GET_ALL_PROVINCES_ERROR:
            {
                return {
                    ...initialState,
                    data: state.data,
                    error: action.payload
                };
            }
        default:
            {
                return state
            }
    }
};

export default provinces;