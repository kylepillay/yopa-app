import * as Actions from '../actions';

const initialState = {
    data: [],
    isLoading: false,
    error  : false
};

const categories = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_ALL_CATEGORIES_SUCCESS:
        {
            return {
                ...initialState,
                data: action.payload.categories
            };
        }
        case Actions.GET_ALL_CATEGORIES_PENDING:
        {
            return {
                ...initialState,
                isLoading: true,
            };
        }
        case Actions.GET_ALL_CATEGORIES_ERROR:
        {
            return {
                ...initialState,
                error  : action.payload
            };
        }
        default:
        {
            return state
        }
    }
};

export default categories;