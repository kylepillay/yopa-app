import * as Actions from '../actions';

const initialState = {
    data: [],
    isLoading: false,
    isRefreshing: false,
    total: '(Loading)',
    error: false
};

const search_deals = function (state = initialState, action) {
    switch (action.type) {
        case Actions.SEARCH_LOCATION_DEALS_SUCCESS:
            {
                return {
                    ...initialState,
                    data: action.payload.data.deals
                };
            }
        case Actions.SEARCH_LOCATION_DEALS_PENDING:
            {
                return {
                    ...initialState,
                    data: state.data,
                    isLoading: true,
                    isRefreshing: true
                };
            }
        case Actions.SEARCH_LOCATION_DEALS_ERROR:
            {
                return {
                    ...initialState,
                    data: state.data,
                    error: action.payload
                };
            }
        case Actions.SEARCH_CATEGORY_DEALS_SUCCESS:
            {
                return {
                    ...initialState,
                    data: action.payload.data.deals
                };
            }
        case Actions.SEARCH_CATEGORY_DEALS_PENDING:
            {
                return {
                    ...initialState,
                    data: state.data,
                    isLoading: true,
                    isRefreshing: true
                };
            }
        case Actions.SEARCH_CATEGORY_DEALS_ERROR:
            {
                return {
                    ...initialState,
                    data: state.data,
                    error: action.payload
                };
            }
        case Actions.SEARCH_PROVINCE_DEALS_SUCCESS:
            {
                return {
                    ...initialState,
                    data: action.payload.data.deals
                };
            }
        case Actions.SEARCH_PROVINCE_DEALS_PENDING:
            {
                return {
                    ...initialState,
                    data: state.data,
                    isLoading: true,
                    isRefreshing: true
                };
            }
        case Actions.SEARCH_PROVINCE_DEALS_ERROR:
            {
                return {
                    ...initialState,
                    data: state.data,
                    error: action.payload
                };
            }
        case Actions.SEARCH_TEXT_DEALS_SUCCESS:
            {
                return {
                    ...initialState,
                    data: action.payload.data.deals
                };
            }
        case Actions.SEARCH_TEXT_DEALS_PENDING:
            {
                return {
                    ...initialState,
                    data: state.data,
                    isLoading: true,
                    isRefreshing: true
                };
            }
        case Actions.SEARCH_TEXT_DEALS_ERROR:
            {
                return {
                    ...initialState,
                    data: state.data,
                    error: action.payload
                };
            }
        case Actions.CLEAR_SEARCH:
            {
                return {
                    ...initialState
                };
            }
        case Actions.UPDATE_DEAL_FAVOURITE_SEARCH:
            {
                return {
                    ...state,
                    data: state.data.map(
                        (item, i) => item.id === action.payload.id ? { ...item, favourited: action.payload.favourited }
                            : item
                    )
                }
            }
        case Actions.UPDATE_DEAL_SMASHED_SEARCH:
            {
                return {
                    ...state,
                    data: state.data.map(
                        (item, i) => item.id === action.payload.id ? { ...item, smashed: true }
                            : item
                    )
                }
            }
        default:
            {
                return state
            }
    }
};

export default search_deals;