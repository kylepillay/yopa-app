import * as Actions from '../actions';

const initialState = {
    data: false,
    isLoading: false,
    error  : false
};

const grab_deal = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GRAB_DEAL_SUCCESS:
        {
            return {
                ...initialState,
                data: action.payload
            };
        }
        case Actions.GRAB_DEAL_PENDING:
        {
            return {
                ...initialState,
                isLoading: true
            };
        }
        case Actions.GRAB_DEAL_ERROR:
        {
            return {
                ...initialState,
                error  : action.payload
            };
        }
        case Actions.GRAB_DEAL_RESET:
        {
            return {
                ...initialState
            };
        }
        default:
        {
            return state
        }
    }
};

export default grab_deal;