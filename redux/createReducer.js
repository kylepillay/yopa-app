import {combineReducers} from 'redux';
import auth from './auth/reducers';
import deals from './deals/reducers';
import location from './location/reducers';
import alerts from './alerts/reducers';

const createReducer = (asyncReducers) =>
    combineReducers({
        auth,
        deals,
        location,
        alerts,
        ...asyncReducers
    });

export default createReducer;