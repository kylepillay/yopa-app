import * as Actions from '../actions';

const initialState = {
    coords: false,
    servicesEnabled: false,
    isLoading: false,
    error  : false
};

const location = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_LOCATION_SUCCESS:
        {
            return {
                ...initialState,
                servicesEnabled: true,
                coords: action.payload.coords,
            };
        }
        case Actions.GET_LOCATION_PENDING:
        {
            return {
                ...initialState,
                isLoading: true
            };
        }
        case Actions.GET_LOCATION_ERROR:
        {
            return {
                ...initialState,
                servicesEnabled: false,
                error  : action.payload
            };
        }
        default:
        {
            return state
        }
    }
};

export default location;