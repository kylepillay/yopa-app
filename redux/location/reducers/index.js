import {combineReducers} from 'redux';
import location from './location.reducer';

const locationReducers = combineReducers({
    location
});

export default locationReducers;