import * as Location from 'expo-location';
import { Alert } from 'react-native';

export const GET_LOCATION_ERROR = 'GET_LOCATION_ERROR';
export const GET_LOCATION_PENDING = 'GET_LOCATION_PENDING';
export const GET_LOCATION_SUCCESS = 'GET_LOCATION_SUCCESS';

export function getLocation() {
  return (dispatch) => {
    dispatch({
      type: GET_LOCATION_PENDING,
      payload: true
    });

    return _getLocationAsync()
      .then((data) => {
        console.log(data)
        return dispatch({
          type: GET_LOCATION_SUCCESS,
          payload: data
        });
      }
      )
      .catch(error => {
        return dispatch({
          type: GET_LOCATION_ERROR,
          payload: error
        });
      });
  }
}

async function _getLocationAsync() {

  const { status } = await Location.requestForegroundPermissionsAsync();
  if (status === 'granted') {
    return Location.getCurrentPositionAsync({ enableHighAccuracy: true });
  } else {
    Alert.alert('Location Services Not Enabled', 'Please enable location services for this app in your device settings to use all features of this app.');
    return new Error('Location permission not granted');
  }
}
