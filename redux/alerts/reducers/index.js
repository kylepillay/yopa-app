import {combineReducers} from 'redux';
import modal from './modal.reducer'

const alerts = combineReducers({
    modal
});

export default alerts;