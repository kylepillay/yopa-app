import * as Actions from '../actions/index';

const initialState = {
    open  : false,
    options: {
        timeout: 6000,
        message: '',
        title: '',
        errors: false,
        variant: null
    }
};

const modal = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.SHOW_MODAL:
        {
            return {
                open  : true,
                options: {
                    ...initialState.options,
                    ...action.payload
                }
            };
        }
        case Actions.HIDE_MODAL:
        {
            return {
                ...state,
                open: false
            };
        }
        default:
        {
            return state;
        }
    }
};

export default modal;
