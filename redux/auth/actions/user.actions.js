import authService from '../../../services/authService';

export const UPDATE_USER_ERROR = 'UPDATE_USER_ERROR';
export const UPDATE_USER_PENDING = 'UPDATE_USER_PENDING';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';

export const SET_USER_DATA = 'SET_USER_DATA';
export const REMOVE_USER_DATA = 'REMOVE_USER_DATA';
export const UPDATE_USER_RESET = 'UPDATE_USER_RESET';

/**
 * Set User Data
 */
export function setUserData(user) {
    return (dispatch) => {
        dispatch({
            type: SET_USER_DATA,
            payload: user
        })
    }
}

export function logoutUser() {

    return (dispatch) => {

        authService.logout();

        dispatch({
            type: REMOVE_USER_DATA
        })
    }
}

export function updateUserData(user) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USER_PENDING,
            payload: true
        });
        return authService.updateUserData(user)
            .then((user) => {

                dispatch(setUserData(user));

                dispatch(showModal({ title: 'User updated successfully' }));

                return dispatch({
                    type: UPDATE_USER_SUCCESS,
                    payload: user
                });
            }
            )
            .catch(error => {
                return dispatch({
                    type: UPDATE_USER_ERROR,
                    payload: error
                });
            });
    }
}

export function resetUpdateUser() {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USER_RESET,
            payload: {}
        })
    }
}
