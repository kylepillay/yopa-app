import authService from '../../../services/authService';

export const GET_ALL_FAVOURITES_ERROR = 'GET_ALL_FAVOURITES_ERROR';
export const GET_ALL_FAVOURITES_PENDING = 'GET_ALL_FAVOURITES_PENDING';
export const GET_ALL_FAVOURITES_SUCCESS = 'GET_ALL_FAVOURITES_SUCCESS';
export const UPDATE_DEAL_FAVOURITE_FAVOURITES = 'UPDATE_DEAL_FAVOURITE_FAVOURITES';
export const UPDATE_DEAL_SMASHED_FAVOURITES = 'UPDATE_DEAL_SMASHED_FAVOURITES';

export function getAllFavourites() {
  return (dispatch) => {
    dispatch({
      type: GET_ALL_FAVOURITES_PENDING,
      payload: true
    });
    return authService.getAllFavourites()
      .then((data) => {
          return dispatch({
            type: GET_ALL_FAVOURITES_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: GET_ALL_FAVOURITES_ERROR,
          payload: error
        });
      });
  }
}
