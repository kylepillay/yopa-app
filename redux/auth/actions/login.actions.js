import authService from '../../../services/authService';
import { setUserData } from './user.actions';
import { showModal } from '../../alerts/actions';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_PENDING = 'LOGIN_PENDING';

export function submitLogin({ email, password, device_name }) {
    return (dispatch) => {
        dispatch({
            type: LOGIN_PENDING,
            payload: true
        });
        return authService.signInWithEmailAndPassword(email, password, device_name)
            .then((user) => {
                dispatch(setUserData(user));

                dispatch(showModal({ title: 'Login Successful', message: `Welcome, ${user.name}` }));

                return dispatch({
                    type: LOGIN_SUCCESS
                });
            }
            )
            .catch(error => {
                dispatch(showModal({ title: "Login Error", errors: error?.data?.errors ? error?.data?.errors : {"Invalid Credentials": "You have entered an incorrect username or password or your account does not exist."} }));

                return dispatch({
                    type: LOGIN_ERROR,
                    payload: error?.data?.errors ? error.data.errors : error?.data?.message
                });
            });
    }
}
