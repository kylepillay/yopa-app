import authService from '../../../services/authService';
import { showModal } from '../../alerts/actions';

export const FORGOT_ERROR = 'FORGOT_ERROR';
export const FORGOT_SUCCESS = 'FORGOT_SUCCESS';
export const FORGOT_PENDING = 'FORGOT_PENDING';

export function submitForgot({ email }) {
    return (dispatch) => {
        dispatch({
            type: FORGOT_PENDING,
            payload: true
        });
        return authService.resetPassword(email)
            .then(() => {

                dispatch(showModal({ title: 'Success!', message: 'Instructions on resetting your password have been emailed to you' }));

                return dispatch({
                    type: FORGOT_SUCCESS
                });
            })
            .catch(error => {

                dispatch(showModal({ title: 'Something Went Wrong', message: error.data.message }));

                return dispatch({
                    type: FORGOT_ERROR,
                    payload: error.data.errors ? error.data.errors : error.data.message
                });
            });
    }
}
