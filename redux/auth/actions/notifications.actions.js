import authService from '../../../services/authService';
import {UPDATE_DEAL_FAVOURITE_FAVOURITES} from "./favourites.actions";

export const GET_ALL_NOTIFICATIONS_ERROR = 'GET_ALL_NOTIFICATIONS_ERROR';
export const GET_ALL_NOTIFICATIONS_PENDING = 'GET_ALL_NOTIFICATIONS_PENDING';
export const GET_ALL_NOTIFICATIONS_SUCCESS = 'GET_ALL_NOTIFICATIONS_SUCCESS';

export const CHECK_NOTIFICATIONS_ERROR = 'CHECK_NOTIFICATIONS_ERROR';
export const CHECK_NOTIFICATIONS_PENDING = 'CHECK_NOTIFICATIONS_PENDING';
export const CHECK_NOTIFICATIONS_SUCCESS = 'CHECK_NOTIFICATIONS_SUCCESS';

export const REMOVE_NOTIFICATION_ERROR = 'REMOVE_NOTIFICATION_ERROR';
export const REMOVE_NOTIFICATION_PENDING = 'REMOVE_NOTIFICATION_PENDING';
export const REMOVE_NOTIFICATION_SUCCESS = 'REMOVE_NOTIFICATION_SUCCESS';
export const REMOVE_NOTIFICATION_RESET = 'REMOVE_NOTIFICATION_RESET';

export function getAllNotifications() {
  return (dispatch) => {
    dispatch({
      type: GET_ALL_NOTIFICATIONS_PENDING,
      payload: true
    });
    return authService.getAllNotifications()
      .then((data) => {
          return dispatch({
            type: GET_ALL_NOTIFICATIONS_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: GET_ALL_NOTIFICATIONS_ERROR,
          payload: error
        });
      });
  }
}

export function checkNotifications(latitude, longitude) {
  return (dispatch) => {
    dispatch({
      type: CHECK_NOTIFICATIONS_PENDING,
      payload: true
    });
    return authService.checkNotifications(latitude, longitude)
      .then((data) => {
          return dispatch({
            type: CHECK_NOTIFICATIONS_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: CHECK_NOTIFICATIONS_ERROR,
          payload: error
        });
      });
  }
}

export function removeNotification(notificationId) {
  return (dispatch) => {
    dispatch({
      type: REMOVE_NOTIFICATION_PENDING,
      payload: true
    });
    return authService.removeNotification(notificationId)
      .then((data) => {
          return dispatch({
            type: REMOVE_NOTIFICATION_SUCCESS,
            payload: data
          });
        }
      )
      .catch(error => {
        return dispatch({
          type: REMOVE_NOTIFICATION_ERROR,
          payload: error
        });
      });
  }
}

export function removeNotificationReset(deal) {
  return (dispatch) => {
    return dispatch({
      type: REMOVE_NOTIFICATION_RESET,
      payload: {}
    });
  }
}
