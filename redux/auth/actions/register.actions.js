import { showModal } from '../../alerts/actions';
import authService from '../../../services/authService';

export const REGISTER_ERROR = 'REGISTER_ERROR';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_PENDING = 'REGISTER_PENDING';

export function submitRegister({ name, password, email, password_confirmation }) {
  return (dispatch) => {
    dispatch({
      type: REGISTER_PENDING,
      payload: true
    });

    authService.createUser({
      name,
      email,
      password,
      password_confirmation,
      role: 'user'
    }).then((data) => {

      dispatch(showModal({ message: 'Registration Successful! You may now log in.' }));

      return dispatch({
        type: REGISTER_SUCCESS
      });
    }
    ).catch(error => {

      dispatch(showModal({ title: error.data.message, errors: error.data.errors }));

      return dispatch({
        type: REGISTER_ERROR,
        payload: error.data.errors ? error.data.errors : error.data.message
      });
    });
  }
}
