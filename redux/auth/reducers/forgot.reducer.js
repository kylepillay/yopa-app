import * as Actions from '../actions';

const initialState = {
    success: false,
    error  : false,
    isLoading: false
};

const forgot = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.FORGOT_SUCCESS:
        {
            return {
                ...initialState,
                success: true
            };
        }
        case Actions.FORGOT_PENDING:
        {
            return {
                ...initialState,
                isLoading: true
            };
        }
        case Actions.FORGOT_ERROR:
        {
            return {
                ...initialState,
                error  : action.payload
            };
        }
        default:
        {
            return state
        }
    }
};

export default forgot;