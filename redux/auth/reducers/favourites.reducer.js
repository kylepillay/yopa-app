import _ from 'lodash';
import * as Actions from '../actions';

const initialState = {
    data: [],
    isLoading: false,
    error  : false
};

const favourites = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_ALL_FAVOURITES_SUCCESS:
        {
            return {
                ...initialState,
                data: action.payload.favourites
            };
        }
        case Actions.GET_ALL_FAVOURITES_PENDING:
        {
            return {
                ...initialState,
                isLoading: true,
            };
        }
        case Actions.GET_ALL_FAVOURITES_ERROR:
        {
            return {
                ...initialState,
                error  : action.payload
            };
        }
        case Actions.UPDATE_DEAL_SMASHED_FAVOURITES:
        {
            return {
                ...state,
                data: state.data.map(
                  (item, i) => item.id === action.payload.id ? {...item, smashed: true}
                    : item
                )
            }
        }
        default:
        {
            return state
        }
    }
};

export default favourites;