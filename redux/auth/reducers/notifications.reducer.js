import * as Actions from '../actions';

const initialState = {
    data: [],
    checkData: null,
    isLoading: false,
    isLoadingCheck: false,
    removeData: false,
    errorRemove: false,
    isLoadingRemove: false,
    errorCheck: false,
    error: false
};

const notifications = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_ALL_NOTIFICATIONS_SUCCESS:
            {
                return {
                    ...state,
                    data: action.payload.notifications,
                    isLoading: false
                };
            }
        case Actions.GET_ALL_NOTIFICATIONS_PENDING:
            {
                return {
                    ...state,
                    isLoading: true
                };
            }
        case Actions.GET_ALL_NOTIFICATIONS_ERROR:
            {
                return {
                    ...state,
                    error: action.payload,
                    isLoading: false
                };
            }
        case Actions.CHECK_NOTIFICATIONS_SUCCESS:
            {
                return {
                    ...state,
                    checkData: action.payload
                };
            }
        case Actions.CHECK_NOTIFICATIONS_PENDING:
            {
                return {
                    ...state,
                    isLoadingCheck: true,
                };
            }
        case Actions.CHECK_NOTIFICATIONS_ERROR:
            {
                return {
                    ...state,
                    errorCheck: action.payload
                };
            }
        case Actions.REMOVE_NOTIFICATION_SUCCESS:
            {
                return {
                    ...state,
                    removeData: true
                };
            }
        case Actions.REMOVE_NOTIFICATION_PENDING:
            {
                return {
                    ...state,
                    isLoadingRemove: true,
                };
            }
        case Actions.REMOVE_NOTIFICATION_ERROR:
            {
                return {
                    ...state,
                    errorRemove: action.payload
                };
            }
        case Actions.REMOVE_NOTIFICATION_RESET:
            {
                return {
                    ...state,
                    data: state.data
                };
            }
        default:
            {
                return state
            }
    }
};

export default notifications;