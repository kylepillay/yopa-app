import * as Actions from '../actions';

const initialState = {
    data: {},
    success: false,
    isLoading: false,
    error: false
};

const user = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.SET_USER_DATA:
        {
            return {
                ...initialState,
                data: action.payload
            };
        }
        case Actions.REMOVE_USER_DATA:
        {
            return initialState;
        }
        case Actions.UPDATE_USER_SUCCESS:
        {
            return {
                ...state,
                success: true,
                error: false,
                isLoading: false
            };
        }
        case Actions.UPDATE_USER_PENDING:
        {
            return {
                ...state,
                success: false,
                error: false,
                isLoading: true
            };
        }
        case Actions.UPDATE_USER_ERROR:
        {
            return {
                ...state,
                success: false,
                error: action.payload,
                isLoading: false
            };
        }
        case Actions.UPDATE_USER_RESET:
        {
            return {
                ...state,
                success: false
            };
        }
        default:
        {
            return state
        }
    }
};

export default user;
