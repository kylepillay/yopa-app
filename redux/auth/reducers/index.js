import {combineReducers} from 'redux';
import user from './user.reducer';
import login from './login.reducer';
import register from './register.reducer';
import notifications from './notifications.reducer';
import favourites from './favourites.reducer';
import forgot from './forgot.reducer';

const authReducers = combineReducers({
    user,
    login,
    register,
    notifications,
    favourites,
    forgot
});

export default authReducers;