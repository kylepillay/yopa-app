export default {
    colors: {
        primary: '#439baa',
        textLight: '#ffffff',
        textMedium: '#c3c3c3',
        background: '#fbfbfb'
    }
}