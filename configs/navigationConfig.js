const navigationConfig = [
    {
        'id'      : 'applications',
        'title'   : 'Applications',
        'type'    : 'group',
        'icon'    : 'apps',
        'children': [
            {
                'id'      : 'dashboards',
                'title'   : 'Dashboards',
                'type'    : 'collapse',
                'icon'    : 'dashboard',
                'children': [
                    {
                        'id'   : 'project-dashboard',
                        'title': 'Summary',
                        'type' : 'item',
                        'url'  : '/apps/dashboards/project'
                    }
                ]
            },
            {
                'type': 'divider',
                'id'  : 'divider-1'
            },
            {
                'id'      : 'deals',
                'title'   : 'Deals',
                'type'    : 'collapse',
                'icon'    : 'shopping_cart',
                'url'     : '/apps/deals',
                'children': [
                    {
                        'id'   : 'e-commerce-deals',
                        'title': 'All Deals',
                        'type' : 'item',
                        'url'  : '/apps/e-commerce/deals',
                        'exact': true
                    },
                    {
                        'id'   : 'e-commerce-new-deal',
                        'title': 'New Deal',
                        'type' : 'item',
                        'url'  : '/apps/e-commerce/deals/new',
                        'exact': true
                    },
                ]
            },
            {
                'id'      : 'stores',
                'title'   : 'Stores',
                'type'    : 'collapse',
                'icon'    : 'store',
                'url'     : '/apps/stores',
                'children': [
                    {
                        'id'   : 'e-commerce-stores',
                        'title': 'All Stores',
                        'type' : 'item',
                        'url'  : '/apps/e-commerce/stores',
                        'exact': true
                    },
                    {
                        'id'   : 'e-commerce-new-store',
                        'title': 'New Store',
                        'type' : 'item',
                        'url'  : '/apps/e-commerce/stores/new',
                        'exact': true
                    }
                ]
            }
        ]
    }
];

export default navigationConfig;
