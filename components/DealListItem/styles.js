import { StyleSheet } from "react-native";

export default StyleSheet.create({
  mainContainer: {
    marginVertical: 10,
    width: '100%'
  },
  cardContainer: {
    borderWidth: 0
  },
  cardHeaderContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  cardHeader: {
    flex: 1,
    color: '#439baa',
    fontSize: 22,
    marginLeft: 5,
    alignSelf: 'flex-start',
    marginBottom: 10,
    fontFamily: 'Rubik-Black'
  },
  cardHeaderIcon: {
    flex: 1,
    alignSelf: 'flex-end'
  },
  iconActivityIndicator: {
    marginTop: -15
  },
  topButtonsContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10
  },
  topButton: {
    width: '50%',
    backgroundColor: '#ffffff',
    height: 40,
    borderWidth: 1,
    borderColor: '#439baa',
    borderRadius: 40,
    marginRight: 5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  topButtonText: {
    fontFamily: 'Rubik-Medium',
    color: '#439baa',
    fontSize: 18
  },
  mainButton: {
    borderRadius: 5,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    marginTop: 10,
    backgroundColor: '#439baa'
  }
});