import React, { Component, useCallback, useEffect, useState } from 'react';
import {
  Text,
  Image,
  View,
  ActivityIndicator
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { Button, Card, Icon } from "react-native-elements";

import styles from './styles';
import { favouriteDeal, favouriteDealReset, updateDealFavourite, smashDealReset, updateDealSmashed, grabDealReset, updateDealGrabbed } from '../../redux/deals/actions';


const DealListItem = ({ navigation, deal }) => {

  const dispatch = useDispatch()

  const isLoadingFavourite = useSelector(s => s.deals.favourite_deal.isLoading)
  const favouriteDealSuccess = useSelector(s => s.deals.favourite_deal.data)
  const favouriteDealError = useSelector(s => s.deals.favourite_deal.error)
  const smashDealSuccess = useSelector(s => s.deals.smash_deal.data)
  const grabDealSuccess = useSelector(s => s.deals.grab_deal.data)
  const [errorMessage, setErrorMessage] = useState({})

  const _navigateToDeal = () => {
    navigation.navigate('Deal', { deal });
  }

  useEffect(() => {
    if (favouriteDealError) {
      setErrorMessage(favouriteDealError);
      dispatch(favouriteDealReset())
    }
  }, [favouriteDealError])

  useEffect(() => {
    if (favouriteDealSuccess) {
      dispatch(updateDealFavourite(favouriteDealSuccess.deal))
      dispatch(favouriteDealReset())
    }
  }, [favouriteDealSuccess])

  useEffect(() => {
    if (smashDealSuccess) {
      dispatch(smashDealReset());
      dispatch(updateDealSmashed(smashDealSuccess.deal));
    }
  }, [smashDealSuccess])

  useEffect(() => {
    if (grabDealSuccess) {
      dispatch(updateDealGrabbed(grabDealSuccess.deal));
      dispatch(grabDealReset());
    }
  }, [grabDealSuccess])

  return (
    <View style={styles.mainContainer} >
      <Card containerStyle={styles.cardContainer} >
        <View style={styles.cardHeaderContainer} >
          <Text style={styles.cardHeader}>{deal.name}</Text>
          {!isLoadingFavourite | !deal ? <Icon
            type='ionicon'
            name={deal.favourited ? 'ios-heart' : 'ios-heart-outline'}
            onPress={() => {
              dispatch(favouriteDeal(deal))
            }}
            style={styles.cardHeaderIcon}
            color='#439baa'
          /> : <ActivityIndicator animating size="small" color={'#439baa'} />}
        </View>

        <View style={styles.topButtonsContainer} >
          <View style={styles.topButton}>
            <Text style={styles.topButtonText}>
              {'Was: R' + deal.price}
            </Text>
          </View>
          <View style={{ ...styles.topButton, backgroundColor: '#439baa' }}>
            <Text style={{ ...styles.topButtonText, color: '#ffffff' }}>
              {'Now: R' + deal.salePrice}
            </Text>
          </View>
        </View>
        <Image
          source={{
            uri: `${deal.imageurl}`
          }}
          style={{ height: 200 }}
        />
        <Button
          onPress={_navigateToDeal}
          buttonStyle={styles.mainButton}
          title='View Deal'
        />
      </Card>
    </View>
  );
}

export default DealListItem;