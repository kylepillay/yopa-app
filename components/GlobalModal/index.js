import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from "react-native-elements";
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { hideModal } from '../../redux/alerts/actions';

const variantIcon = {
    success: "tick-circle",
    warning: "warning-sign",
    error: "error",
    info: "info-sign"
};

class GlobalModal extends Component {

    closeModal() {
        this.props.hideModal();
    }

    render() {
        let { options, open } = this.props;

        return (
            <Modal isVisible={open}>
                <View style={styles.container}>
                    <Text style={styles.modalHeader}>{options.title}</Text>
                    {options.errors && options.errors.length && typeof options.errors[0] === 'object' ?
                        Object.keys(options.errors).map(function (key) {
                            return (<Text key={key} style={styles.errorText}>{`${key.charAt(0).toUpperCase() + key.slice(1)}:\n`}
                                {options.errors[key].map((item, index) => {
                                    return (<Text key={index} style={styles.modalText}>{`${item}\n`}</Text>)
                                })}
                            </Text>)
                        })
                    : options.errors && options.errors.length && options.errors.map((item, index) => {
                        return (<Text key={index} style={styles.modalText}>{`${item}\n`}</Text>)
                    })}
                    {!!options.message && <Text style={styles.modalTextBottom}>{options.message}</Text>}
                    <Button
                        onPress={this.closeModal.bind(this)}
                        type="outline"
                        titleStyle={styles.buttonTitle}
                        containerStyle={styles.buttonContainer}
                        buttonStyle={styles.button}
                        raised
                        title='OKAY'
                    />
                </View>
            </Modal>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        hideModal
    }, dispatch);
}

function mapStateToProps({ alerts }) {
    return {
        open: alerts.modal.open,
        options: alerts.modal.options
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 30,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        borderRadius: 10
    },
    modalHeader: {
        color: '#2d2d2d',
        fontSize: 22,
        marginBottom: 20,
        fontFamily: 'Rubik-Black'
    },
    modalText: {
        color: '#2d2d2d',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    modalTextBottom: {
        marginTop: 5,
        color: '#2d2d2d',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    errorText: {
        color: '#cf111a',
        fontSize: 18,
        fontFamily: 'Rubik-Regular'
    },
    buttonContainer: {
        marginTop: 25
    },
    button: {
        paddingHorizontal: 20
    },
    buttonTitle: {
        marginBottom: 2,
        color: '#439baa'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(GlobalModal);
