import React from 'react'
import { StyleSheet } from 'react-native';
import { Input, Icon } from 'react-native-elements'

export default React.forwardRef((props, ref) => {
    return <Input
        inputStyle={{ color: '#fff' }}
        inputContainerStyle={{ borderColor: '#fff' }}
        placeholderTextColor="#cccccc"
        containerStyle={{ marginVertical: 15 }}
        errorStyle={styles.errorStyle}
        blurOnSubmit
        ref={ref}
        leftIcon={<Icon name={props.iconName} containerStyle={styles.iconContainer} type='ionicon' size={24} color='#fff' />}
        {...props}
    />
})

const styles = StyleSheet.create({
    errorStyle: {
        fontFamily: 'Rubik-Regular',
        color: '#d60404',
        fontSize: 14
    },
    iconContainer: {
        marginRight: 15
    }
});
